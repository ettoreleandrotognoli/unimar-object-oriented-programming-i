# Geradores

```python

def my_range(start, end):
    current = start
    while current < end:
        yield current
        current += 1

```
