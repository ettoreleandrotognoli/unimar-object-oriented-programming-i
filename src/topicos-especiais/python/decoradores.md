# Decoradores

Não necessariamente implementa o padrão [Decorator](https://refactoring.guru/design-patterns/decorator)

```python,ocirun
def soma(a, b):
    return a + b

def soma_debug(a, b):
    r = soma(a, b)
    print(f'soma({a}, {b}) -> {r}')
    return r

soma_debug(2,3)
```

```python,ocirun
def debug(fn):
    def wrapper(a, b):
        r = fn(a, b)
        print(f'{fn.__name__}({a}, {b}) -> {r}')
        return r
    return wrapper

def soma(a, b):
    return a + b

soma = debug(soma)

soma(2, 3)
```

```python,ocirun
def debug(fn):
    def wrapper(a, b):
        r = fn(a, b)
        print(f'{fn.__name__}({a}, {b}) -> {r}')
        return r
    return wrapper


@debug
def soma(a, b):
    return a + b

soma(2, 3)
```

```python,ocirun
def debug(fn):
    def wrapper(*args, **kwargs):
        r = fn(*args,**kwargs)
        print(f'{fn.__name__}(*{args}, **{kwargs}) -> {r}')
        return r
    return wrapper


@debug
def soma(a, b):
    return a + b

@debug
def hello(name: str):
    print(f'Hello, {name}')

soma(2, 3)
hello('World')
```
