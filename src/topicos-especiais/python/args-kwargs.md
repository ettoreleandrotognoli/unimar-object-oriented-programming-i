# *args e **kwargs

## Parâmetros Ordenados

```python,ocirun
def soma(a, b):
    return a + b

print(soma(2,3))

parametros = [ 2, 3 ]

print(soma(*parametros))
```

```python,ocirun
def soma(*args):
    acc = 0
    for it in args:
        acc += it
    return acc

print(soma( 2, 3))
print(soma( 2, 3, 5, 7))
```

## Parâmetros Nomeados

```python,ocirun
def sub(a, b):
    return a - b

print(sub(2,3))
print(sub(a=2, b=3))
print(sub(b=3, a=2))

parametros = {
    'a' : 2,
    'b': 3,
}

print(sub(**parametros))
```

## Ambos

```python,ocirun
def debug(*args, **kwargs):
    print(args)
    print(kwargs)
    print('-----')


debug(1, 2, 3, 4)
debug(a=1,b=2,c=3,d=4)
debug(1, 2, a=1, b=2)
```

```python,ocirun
def soma(*args, **kwargs):
    acc = 0
    for it in args:
        acc += it
    for it in kwargs.values():
        acc += it
    return acc

print(soma(1, 2, 3, a=1, b=2, c=3))
```
