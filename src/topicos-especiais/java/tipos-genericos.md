# Tipos Genéricos

```java,ocirun
import java.util.List;
import java.util.LinkedList;

public class Main {

    public static void main(String... args) {
        List<String> stringList = new LinkedList<>();
        stringList.add("Fuu");
        stringList.add("Bar");
        List<Integer> intList = new LinkedList<>();
        intList.add(1);
        intList.add(2);
        System.out.println(stringList);
        System.out.println(intList);
    }

}
```

```java,ocirun
public class Main {

    static class Node<E> {
        E value;
        Node<E> next;

        Node(E value, Node<E> next) {
            this.value = value;
            this.next = next;
        }
    }

    static class LinkedStack<E> {
        private Node<E> head = null;
        private int size = 0;

        public void push(E value) {
            Node<E> tail = head;
            head = new Node<>(value, tail);
            size += 1;
        }

        public E pop() {
            if(size == 0) {
                throw new RuntimeException("Stack Underflow");
            }
            E value = head.value;
            head = head.next;
            size -= 1;
            return value;
        }

    }


    public static void main(String... args) {
        LinkedStack<Integer> stack = new LinkedStack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
    }
}
```
