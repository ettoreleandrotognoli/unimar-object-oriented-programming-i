# Ponteiro de Função

```c,ocirun
#include <stdio.h>

int soma(int a, int b) {
    return a + b;
}

int soma_debug(int a, int b) {
    int result = a + b;
    printf("%d + %d = %d\n", a, b, result);
    return result;
}


typedef int (*t_soma)(int, int);


int main(int argc, char** arvg) {
    t_soma fn = soma_debug;
    fn(10, 10);
    return 0;
}
```

## Interfaces e Polimorfismo

```c,ocirun
#include <stdio.h>

typedef struct {
  int (*sum)(int, int);
  int (*sub)(int, int);
  int (*mul)(int, int);
  int (*div)(int, int);
} Calc;


int sum(int a, int b) {
    return a + b;
}

int sub(int a, int b) {
    return a - b;
}

int mul(int a, int b) {
  return a * b;
}

int div(int a, int b) {
  return a / b;
}

Calc create_calc() {
    Calc calc = {
        .sum=sum,
        .sub=sub,
        .mul=mul,
        .div=div
    };
    return calc;
}


int debug_sum(int a, int b) {
  int result = a + b;
  printf("%d + %d = %d\n", a, b, result);
  return result;
}

int debug_sub(int a, int b) {
  int result = a - b;
  printf("%d - %d = %d\n", a, b, result);
  return result;
}

int debug_mul(int a, int b) {
  int result = a * b;
  printf("%d * %d = %d\n", a, b, result);
  return result;
}

int debug_div(int a, int b) {
  int result = a / b;
  printf("%d / %d = %d\n", a, b, result);
  return result;
}

Calc create_debug_calc() {
  Calc calc = {
      .sum = debug_sum,
      .sub = debug_sub,
      .mul = debug_mul,
      .div = debug_div
    };
  return calc;
}

int main(int argc, char **argv) {
  Calc calc = create_debug_calc();
  calc.sum(10, 10);
  calc = create_calc();
  printf("%d\n", calc.sum(10, 10));
  return 0;
}
```

## "Objetos"

```c,ocirun
#include <stdio.h>

typedef struct Person Person;

struct Person {
  char *name;
  void (*hello)(Person *);
};

void hello(Person *self) { printf("Hello %s\n", self->name); }

void hello_john_snow(Person *self) {
  printf("You know nothing %s\n", self->name);
}

Person create_person(char *name) {
  Person p = {
      .name = name,
      .hello = hello,
  };
  return p;
}

int main(int argc, char **argv) {
  Person person = create_person("John Snow");
  person.hello(&person);
  person.hello = hello_john_snow;
  person.hello(&person);
}
```
