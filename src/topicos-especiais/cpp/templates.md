# Templates

## Sem Template =(

```cpp,ocirun
#include <iostream>

int my_div(int a, int b)  {
    return a / b;
}

float my_div(float a, float b)  {
    return a / b;
}

double my_div(double a, double b)  {
    return a / b;
}


int main(int argc, char** argv) {
    std::cout << my_div(10, 3) << std::endl;
    std::cout << my_div(10.f, 3.f) << std::endl;
    std::cout << my_div(10., 3.) << std::endl;
}
```

## Com template =)

```cpp,ocirun
#include <iostream>

template<typename T>
T my_div(T a, T b) {
    return a / b;
}

int main(int argc, char** argv) {
    std::cout << my_div<int>(10, 3) << std::endl;
    std::cout << my_div<float>(10, 3) << std::endl;
    std::cout << my_div<double>(10, 3) << std::endl;
}
```

## Pilha para qualquer tipo

```cpp,ocirun
#include <iostream>

template <typename T> class Node;

template <typename T> class Node {
public:
  T value;
  Node<T> *next;
  Node(T value, Node<T> *next) : value(value), next(next) {}
};

template <typename T> class LinkedStack {
private:
  unsigned int _size = 0;
  Node<T> *head;

public:
  LinkedStack() { head = NULL; }

  void push(T value) {
    this->_size++;
    Node<T> *tail = this->head;
    this->head = new Node<T>(value, tail);
  }

  T pop() {
    if (this->isEmpty()) {
      throw "Stack underflow";
    }
    this->_size--;
    T value = this->head->value;
    Node<T> *tail = this->head->next;
    delete this->head;
    this->head = tail;
    return value;
  }

  bool isEmpty() const { return this->_size == 0; }

  unsigned int size() const { return this->_size; }
};

int main(int argc, char **argv) {
  LinkedStack<int> int_stack;
  int_stack.push(1);
  int_stack.push(2);
  int_stack.push(3);
  std::cout << int_stack.pop() << std::endl;
  std::cout << int_stack.pop() << std::endl;
  std::cout << int_stack.pop() << std::endl;
  LinkedStack<const char *const> str_stack;
  str_stack.push("Number 1");
  str_stack.push("Number 2");
  str_stack.push("Number 3");
  std::cout << str_stack.pop() << std::endl;
  std::cout << str_stack.pop() << std::endl;
  std::cout << str_stack.pop() << std::endl;
  try {
    str_stack.pop();
  } catch (const char *const error) {
    std::cout << error << std::endl;
  }
  return 0;
}
```
