# Orientação a Objetos

Quem nunca utilizou POO deve estar mais familiarizado com o paradigma estruturado,
que é basicamente como aprendemos algoritmos.
Utilizamos sequencias/instruções, seleção/condicional e interação/repetição.
Devemos muito disso ao  [Dijkstra](https://pt.wikipedia.org/wiki/Edsger_Dijkstra), que "condenou" o uso da instrução "GOTO" ( ir para ), no artigo "Go To Statement Considered Harmful".
Na orientação a objetos vamos continuar usando esses recursos, mas vamos adicionar mais alguns conceitos.

1. Abstração

    Simplificar a realidade para representar apenas o necessário

1. Encapsulamento

    Esconder detalhes de implementação e expor interfaces de interação

1. Herança

    Derivar características e interfaces possibilitando reúso

1. Polimorfismo

    Permite que a mesma interface tenha comportamentos diferentes

Esses conceitos não são exclusivos de POO,
podemos fazer coisas bem semelhantes com linguagens que não são consideradas orientada a objetos.

## Abstração

Basicamente usamos abstração quando estamos projetando nosso software,
quando estamos pensando em como resolver os problemas.

> Ada prevê que a invenção de Babbage não só poderia computar números, mas poderia também criar imagens.
> [^ada-unicamp]

[Ada Lovelace](https://pt.wikipedia.org/wiki/Ada_Lovelace) foi uma das primeiras, se não a primeira,
a pensar em abstrair problemas reais e usar números em um computador para resolve-los.

Com orientação a objetos temos alguns recursos para ajudar a abstrair essas informações.
Nesse paradigma podemos criar objetos dentro do nosso programa,
claro que não são objetos físicos,
são objetos virtuais ou abstrações da realidade.
Nesse objetos podemos incluir características dos objetos reais,
assim, podemos fazer cálculos baseados nesses objetos e suas características
e achar a solução para um problema real.
Não se limite apenas a objetos reais,
podemos inventar qualquer tipo de objeto que nos ajude chegar na solução desejada.

## Encapsulamento

Um objeto que conhece o funcionamento interno de outro,
pode acabar se acoplando em detalhes internos,
ficando dependente deles,
e isso complica qualquer tipo de mudança futura.

Imagine um cenário que você aprende a dirigir um carro,
mas ao invés de usar o volante, pedal e cambio,
você acessa diretamente mecanismos que controlam o motor, a transmissão e as rodas.
Você acabou conhecendo os detalhes internos desse carro.
Depois de algum tempo, por um motivo qualquer, você precisa trocar para um carro elétrico.
O funcionamento do motor e da transmissão com certeza vão ser diferentes,
você vai precisar aprender tudo de novo.
No caso de POO significa que você precisaria reescrever o objeto motorista.
Também existe uma grande chance de não sabermos como controlar os detalhes internos da maneira correta,
e termos um resultado insatisfatório.

Usamos o encapsulamentos para evitar que outros acessem esses detalhes internos.

## Herança

A herança serve para reaproveitar caraterísticas entre famílias de objetos.
Basicamente copiamos carateristas que já foram definidas em um objeto para um outro.
Isso acontece do mundo real também, por exemplo, todos os mamíferos tem pelos, glândulas mamárias, diafragma e dentes diferenciados,
logo, um ser humano, que é um mamífero, tem essa caraterísticas e outras mais especificas dos seres humanos,
como polegar opositor.
Ao criar um ser humano, podemos reaproveitar a definição de mamífero e apenas adicionar as características especificas.

Mas a parte mais importante da herança é a garantia das interfaces.
Voltando para o exemplo do carro,
imagine agora que você aprendeu a dirigir usando o volante, pedais e cambio,
podemos chamar esses itens de interface do carro.
Ao criarmos um novo tipo de carro podemos herdar essa interface,
assim, qualquer um que sabe usar um volante, pedais e o cambio,
vai estar apto a dirigir o novo carro.
Você não precisa aprender tudo de novo,
em POO isso significa que ainda podemos usar os mesmo objeto motorista para pilotar qualquer carro com essa interface.

## Polimorfismo

Polimorfismo é quando objetos com a mesma interface podem ter comportamentos diferentes,
é de longe o conceito que mais possibilita o reúso de código.

Pensem na interface USB,
existe uma infinidade de dispositivos diferentes que tem comportamentos diferentes que seguem a mesma interface,
encaixam na mesma porta nos nossos computadores.
Se cada dispositivo precisasse de uma porta diferente, provavelmente nunca iriamos reusar essas portas.

Voltando ao exemplo do carro, os carros tiveram vários tipos de evoluções ao longo dos anos.
Motores mais fortes, mais econômicos, freios ABS, controle de tração e etc.
Mas todos ainda tem a mesma interface, volante e pedais, não precisamos reaprender a dirigir para acompanhar essas evoluções.

Com polimorfismo e interfaces bem definidas conseguimos alterar e extender funcionalidades no nosso sistema sem quebrar as já existentes.

---

[^ada-unicamp]: <http://www.ime.unicamp.br/~apmat/ada-lovelace/>
