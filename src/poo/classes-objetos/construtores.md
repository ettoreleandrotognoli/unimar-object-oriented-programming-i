# Método Construtor

> Na programação orientada a objetos, os métodos construtores são como instruções especiais usadas para criar e inicializar objetos quando são criados a partir de uma classe. Eles são como uma receita que o programa segue quando um novo objeto é criado.
>
> Imagine que você está construindo casas usando um plano. O plano não apenas diz como a casa deve parecer, mas também como ela deve ser montada. Um método construtor é como o plano de montagem para objetos em programação. Ele define como um objeto deve ser configurado assim que é criado.
>
> Os métodos construtores podem ser usados para definir valores iniciais para os atributos de um objeto e para executar tarefas de configuração necessárias. Eles garantem que, sempre que um objeto é criado, ele comece com um estado consistente e pré-definido.
>
> Por exemplo, se você tem uma classe "Pessoa", um método construtor pode definir valores iniciais para os atributos "nome" e "idade" quando um novo objeto "Pessoa" é criado.
>
> Os métodos construtores são uma parte fundamental da criação de objetos em POO, pois permitem que você estabeleça um ponto de partida sólido para cada instância da classe. Eles garantem que os objetos sejam criados da maneira correta, evitando estados inconsistentes ou indefinidos.
> [^chat-gpt]

---

O método construtor é responsável por inicializar o objeto que estamos construindo, cada linguagem pode ter um padrão para o nome desse método.
Em java e C++ o método tem o mesmo nome que a classe, em python usamos [`__init__`](https://docs.python.org/3/reference/datamodel.html?highlight=__init__#object.__init__),
em php usamos [`__construct`](https://www.php.net/manual/en/language.oop5.decon.php), mas a ideia é sempre a mesma, um método que recebe os parâmetros para inicializar o objeto.

Nos exemplos anteriores utilizamos o `@dataclass` e o `@AllArgsConstructor`,
ambos são utilizados para geração automática de código,
inclusive do método construtor e por isso o omitimos,
mas sempre precisamos de um método construtor.
Dependendo da classe e da linguagem de programação o método construtor pode ser gerado automaticamente dando a impressão de que ele não existe.

Nos exemplos seguintes o método construtor foi feito da maneira mais simples possível, e basicamente igual ao gerado automaticamente pelo `@dataclass` e o `@AllArgsConstructor`.

## Python

```python
class Retangulo:

    largura: float
    comprimento: float

    def __init__(self, largura: float, comprimento: float):
        self.largura = largura
        self.comprimento = comprimento


if __name__ == '__main__':
    # criando novo objeto
    a = Retangulo(10, 15)
        
```

## Java

```java
class Retangulo {

    public float largura;
    public float comprimento;

    public Retangulo(float largura, float comprimento) {
        this.largura = largura;
        this.comprimento = comprimento;
    }

    public static void main(String... args) {
        // criando novo objeto
        Retangulo a = new Retangulo(10, 15);
    }
}
```

## PHP

```php
class Retangulo {

    public float $largura;
    public float $comprimento;

    function __construct(float $largura, float $comprimento) {
        $this->largura = $largura;
        $this->comprimento = $comprimento;
    }
}

if (!debug_backtrace()) {
    // criando novo objeto
    $a = new Retangulo(10, 15);
}
```

Com PHP podemos definir os atributos usando os parâmetros do método construtor apenas adicionando o modificador de acesso.
Em typescript temos uma sintaxe bem semelhante.

```php
class Retangulo {

    function __construct(
        public float $largura,
        public float $comprimento
    ) {
    }
}

if (!debug_backtrace()) {
    // criando novo objeto
    $a = new Retangulo(10, 15);
}
```

## C++

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    Retangulo(float largura, float comprimento) {
        this->largura = largura;
        this->comprimento = comprimento;
    }
};

int main(int argc, char** argv) {
    // criando novo objeto
    Retangulo a(10, 15);
}

```

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    Retangulo(float largura, float comprimento) :
        largura(largura),
        comprimento(comprimento) {
    }
};

int main(int argc, char** argv) {
    // criando novo objeto
    Retangulo a(10, 15);
}

```

Para criarmos um novo objeto sempre precisamos fornecer os parâmetros do método construtor.

## Construtor Padrão / Default

No código em C a seguir estamos criando uma variável `valor` do tipo `int`, mas não atribuímos um valor inicial para ela.
Desse modo o valor inicial se torna "aleatório", a memoria foi reservada para a variável e seu valor inicial é apenas a "sujeira" que ainda estava no mesmo endereço.

```c,ocirun
#include <stdio.h>

int main(int argc, char** argv) {
    int valor;
    printf("%d\n", valor);
    return 0;
}
```

Para evitar esse comportamento, que muitas vezes pode levar a erros, inicializamos a variável com o valor que desejamos.
No caso do `int` geralmente é algo simples como um `int valor = 0`,
mas podemos ter casos bem mais complexos quando estamos falando de objetos.

Definir um construtor padrão é simplesmente definir um método construtor que não recebe parâmetros.
Em linguagens de tipagem estática isso é trivial, já que com sobrecarga de métodos podemos definir vários métodos construtores
com parâmetros diferentes.
Mas em linguagens de tipagem dinâmica precisamos definir valores padrões para os parâmetros.

Sobrecarga do método construtor em C++:

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    Retangulo(float largura, float comprimento) :
        largura(largura),
        comprimento(comprimento) {
    }

    Retangulo():
        largura(0),
        comprimento(0) {
    }
};

int main(int argc, char** argv) {
    Retangulo padrao;
}
```

Construtor com parâmetros `default` em python:

```python
class Retangulo:

    largura: float
    comprimento: float

    def __init__(self, largura: float = 0., comprimento: float = 0.):
        self.largura = largura
        self.comprimento = comprimento

```

---

Como dica, mantenha o método construtor simples, ele deve no máximo fornecer alguns valores padrões e realizar pequenas conversões.
Caso seu método construtor comece a ficar complexo, talvez esteja na hora de usar [`factory methods`](https://refactoring.guru/design-patterns/factory-method), ou [`builders`](https://refactoring.guru/design-patterns/builder).

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz quiz-construtores.toml}}

## Exercícios Práticos

Crie construtores para as classes das aulas passadas, Retangulo, Circulo e Quadrado.
Pode apenas remover o `@dataclass` e criar o `__init__`, ou fazer do zero em outra linguagem.

1. Construtor de um circulo.

    Defina um construtor para a classe circulo que receba o raio como parâmetro

1. Construtor um quadrado.

    Defina um construtor para a classe quadrado que receba o tamanho de um lado do quadrado.

---

## Dúvidas frequentes

- O que é o `self` no python?

    > Em Python, o self é uma convenção utilizada dentro de uma classe para se referir ao próprio objeto criado a partir dessa classe.
    > Ele é um parâmetro implícito que indica que um método pertence à instância da classe em que está sendo utilizado.
    >
    > Ao chamar um método em um objeto específico da classe, o Python automaticamente passa a própria instância como o primeiro argumento para o método, usando o parâmetro self. Isso permite que o método acesse e trabalhe com os  dados específicos daquela instância.
    >
    > Em resumo, o self é uma convenção em Python usada como o primeiro parâmetro em métodos de classe, permitindo que o método acesse os atributos e métodos da instância em que está sendo chamado. Ele representa o próprio objeto criado a partir da classe e é uma parte importante da programação orientada a objetos em Python.
    > [^chat-gpt]

    É o equivalente ao `this` em várias outras linguagens.
