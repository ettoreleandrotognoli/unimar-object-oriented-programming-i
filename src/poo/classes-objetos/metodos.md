# Métodos

> Na programação orientada a objetos (POO), os métodos são como as ações que os objetos podem realizar. Assim como as pessoas podem executar diferentes ações, como caminhar, falar ou comer, os objetos em POO podem executar tarefas específicas através dos métodos.
>
> Imagine uma classe "Cachorro" em um programa. Além de ter atributos como nome, raça e idade, os cães também podem executar ações, como latir, correr e abanar o rabo. Essas ações são os métodos do objeto "Cachorro". Os métodos permitem que os objetos realizem tarefas, processem informações e interajam com outros objetos.
>
> Os métodos em POO são como as habilidades que um objeto possui. Eles podem envolver cálculos complexos, interações com o ambiente ou até mesmo a comunicação com outros objetos. Ao criar classes em POO, você define métodos para descrever o que os objetos daquela classe podem fazer.
>
> Por exemplo, se você tem uma classe "Conta Bancária", seus métodos podem incluir "depositar", "sacar" e "verificar saldo". Cada método é uma ação que um objeto da classe pode realizar para executar uma tarefa específica.
>
> Portanto, os métodos são uma parte essencial da POO, permitindo que os objetos tenham comportamentos distintos e realizem ações que são relevantes para o contexto da aplicação.
> [^chat-gpt]
---

Quando estamos abstraindo nosso problema usando objetos, além de definir atributos
também podemos definir métodos.
Os métodos representam ações dos nossos objetos,
por esse motivo, normalmente usamos verbos como nomes.

Em POO basicamente chamamos funções que pertencem a uma classe ou objeto de métodos.
Assim como os atributos, cada objeto pode ter seus próprios métodos.
Mas os métodos são iguais para objetos do mesmo tipo, variando apenas um de seus "parâmetros".
Quando estamos usando métodos sempre temos um parâmetro ou variável que indica quem é o objeto dono do método,
qual objeto está executando a ação.

Para demonstrar a diferença entre funções e métodos leve em consideração o código a seguir:

```python
def calcular_area(x: float, y: float) -> float:
    return x * y
```

Neste exemplo é definida uma função para calcular a área, podemos deduzir isso pelo nome da função,
mas não sabemos, apenas olhando o nome da função, se estamos calculando a área de um triangulo, de um quadrado, de um retângulo ou de qualquer outra geometria.

Para exemplificar vamos continuar utilizando nossos objetos retângulos.

```python
a = Retangulo(5, 25)
b = Retangulo(10, 15)
c = Retangulo(8, 18)

a_area = calcular_area(a.largura, a.comprimento)
b_area = calcular_area(b.largura, b.comprimento)
c_area = calcular_area(c.largura, c.comprimento)
```

Neste ultimo exemplo extraímos os atributos de nossos objetos e o utilizamos como parâmetros da função.
Nada de errado, mas podemos melhorar esse código e deixar menos sujeito a erros e menos verboso.

Erros humanos são muito comuns em códigos, então, quanto menos chances deixarmos para erros melhor.
Nesse caso precisamos repetir as linhas e trocar apenas algumas letras, `a`, `b` e `c` que são os nomes das variáveis,
imagino que a maioria das pessoas apenas faria um `ctrl+c` da primeira linha, depois alguns `ctrl+v`s e por fim trocaria as letras.
Novamente nada de errado, mas temos grandes chances de esquecer de trocar uma letra,
e quanto mais linhas repetidas, mais difícil de perceber a diferença de apenas uma letra.

Uma primeira abordagem, para melhorar isso, é trocar os parâmetros da função.

```python
def calcular_area(r: Retangulo) -> float:
    return r.largura * r.comprimento
```

Agora nossa função recebe um objeto do tipo `Retangulo` e não mais os dois parâmetros, largura e comprimento.
Mesmo sem mudar o nome da função deixamos explicito que estamos calculando a area de um retângulo.
A função acessa os atributos do objeto, largura e comprimento, assim nossa chamada de função fica menos verbosa.

```python
a_area = calcular_area(a)
b_area = calcular_area(b)
c_area = calcular_area(c)
```

Agora que nossa função recebe um objeto do tipo `Retangulo` estamos muito próximos de transformar essa função em um método.
Calcular área pode ser uma ação dos objetos do tipo `Retangulo`, também podemos chamar de comportamento, ou método.
Como todos objetos do tipo `Retangulo` possuem os atributos `largura` e `comprimento`,
eles já tem todas os dados necessários para fazer esse cálculo.

Definimos o método dentro classe, assim todos os objetos dessa classe tem esse mesmo método.
A definição é bem semelhante a definição de uma função.

```python
from dataclasses import dataclass

@dataclass
class Retangulo:
    largura: float
    comprimento: float

    def calcular_area(self) -> float:
        return self.largura * self.comprimento
```

Quando definimos um método em python, precisamos colocar o parâmetro `self`, ele sempre é o primeiro parâmetro.
Esse parâmetro contem uma referencia para o objeto atual, ou seja, de quem queremos calcular a área.
Para utilizarmos esse método o acessamos como um atributo mas adicionamos os `()`, assim como fazemos com funções.
E são apenas o `()` mesmo, não precisamos passar o parâmetro `self`.

```python
a_area = a.calcular_area()
b_area = b.calcular_area()
c_area = c.calcular_area()
```

Quando escrevemos `a.calcular_area()` o método `calcular_area` utiliza o objeto `a` como `self`,
como se estivéssemos escrevendo `calcular_area(a)`.
O mesmo acontece para as demais chamadas,
em `b.calcular_area()` o `self` é o `b` e
em `c.calcular_area()` o `self` é o `c`.

O nome  do parâmetro `self` é apenas uma convenção, poderíamos usar `s`, `this`, ou qualquer outro nome que quisermos.
O python sempre usa o primeiro parâmetro, independentemente do seu nome.

Vamos fazer o mesmo exemplo de `Retangulo` em java.

```java
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Retangulo {
    public float largura;
    public float comprimento;

    public float calcularArea() {
        return this.largura * this.comprimento;
    }
}

```

Em java não precisamos adicionar o parâmetro `self`, mas temos uma variável especial chamada `this` com a referência do objeto atual.

Em C++ temos algo bem parecido.

```cpp

class Retangulo {
public:
    float largura;
    float comprimento;

    float calcularArea() {
        return this->largura * this->comprimento;
    }
}

```

Nesse caso também temos a variável especial `this`, mas em C++, `this` é um ponteiro e não uma referência, por esse motivo usamos `->` no lugar de `.`.

Em PHP temos a variável `$this`, que é uma referência, mas mesmo assim usamos `->`.

```php
<?php

class Retangulo {
    public float $largura;
    public float $comprimento;

    function calcularArea() : float {
        return $this->largura * $this->comprimento;
    }
}

```

Os métodos também podem ser representados nos nossos diagramas UML de classes:

```plantuml
@startuml
class Retangulo {
    + largura: float
    + comprimento: float
    + calcularArea() : float
}
@enduml
```

Nossos métodos também podem receber mais parâmetros, assim como qualquer função.
Para demonstrar segue uma modificação da classe `Retangulo` com métodos para redimensionar o tamanho do retângulo, `escalar` e `escalado`.

```python
<!-- cmdrun cat examples/retangulo-escalar.py -->
```

Diagrama UML atualizado:

```plantuml
@startuml
class Retangulo {
    + largura: float
    + comprimento: float
    + calcularArea() : float
    + escalar(float): Retangulo
    + escalado(float): Retangulo
}
@enduml
```

A ideia desses métodos é garantir a proporção do retângulo,
ao fazermos a multiplicação da largura sempre fazemos a mesma multiplicação para comprimento.
O consumidor dessa classe não precisa lembrar de multiplicar sempre os dois atributos,
apenas usa esses métodos.

## Introdução a Imutabilidade

Neste exemplo temos duas maneiras diferentes de redimensionar um retângulo,
o método `escalar` e o `escalado`.

O método `escalar` modifica os atributos do objeto atual, ou seja,
é um método tipico de um objeto mutável.
Já o método `escalado` cria um novo objeto com os valores desejados,
mantendo o atual intacto, comportamento tipico de um objeto imutável.

Decidir quando devemos usar um objeto mutável ou imutável pode ser complicado,
depende das nossas abstrações e do nosso design.
Mas tenha em mente que objetos imutáveis evitam várias dores de cabeça,
como efeitos colaterais indesejados e condições de corrida.

## Sobrecarga

Assim como as funções, também podemos fazer sobrecarga dos métodos,
mas assim como antes, é um recurso quase que exclusivo das linguagens
de tipagem estática.

## Métodos Estáticos

Assim como os atributos, os métodos estáticos são da classe, não precisam de uma instância.
Ou seja, não vamos ter acesso a variável `this` ou `self`.
Esse é um dos motivos para o famoso método `main` do java ser estático,
a JVM executa esse método sem instanciar nenhum objeto.

Podemos usar métodos estáticos de diversas formas,
mas o uso exagerado geralmente indica um design orientado a objetos ruim, ou inexistente,
fazendo que o código pareça somente estruturado.

Um dos usos mais comuns e garantidos, para não estragar nosso design, é como [métodos fabricas](https://pt.wikipedia.org/wiki/F%C3%A1brica_(programa%C3%A7%C3%A3o_orientada_a_objetos)).
São métodos que tem a responsabilidade de construir/fabricar os objetos de alguma maneira mais especifica.

### Python

Em python, o jeito mais fácil de criar métodos estáticos são usando o decoradores
`@staticmethod` ou o `@classmethod`.

Vou exemplificar criando um método fabrica de `Retangulo`,
esse método vai utilizar a função `input` para usar dados fornecidos pelo usuário.

Com o decorador `@staticmethod`, nosso método não recebe nenhum parâmetro.

```python
<!-- cmdrun cat examples/factory-method/retangulo-static.py -->
```

```console
<!-- ocirun python python examples/factory-method/retangulo-static.py < examples/factory-method/retangulo-in.txt -->
```

Com o decorador `@classmethod`, nosso método se torna um método da classe e não dos objetos.
Dessa forma o primeiro parâmetro, normalmente o `self`, se torna a própria classe, mas por convenção usamos o nome `cls`.

```python
<!-- cmdrun cat examples/factory-method/retangulo-class.py -->
```

```console
<!-- ocirun python python examples/factory-method/retangulo-class.py < examples/factory-method/retangulo-in.txt -->
```

Os resultados são bem semelhantes, mas indico o uso do `@classmethod`.
Como recebemos a classe como parâmetro fica mais fácil fazer modificações,
como mover o método para outra classe ou renomear a classe.
Também tem vantagens relacionadas com herança, mas isso é um assunto para mais a frente.

O método estático no diagrama de classes UML:

```plantuml
@startuml
class Retangulo {
    + {static} from_input(): Retangulo
    + largura: float
    + comprimento: float
    + calcularArea() : float
}
@enduml
```

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Material Complementar

- [Atributos e Métodos Estáticos em PHP](https://www.php.net/manual/en/language.oop5.static.php)  

---

## Exercícios de fixação

{{#quiz quiz-metodos.toml}}

## Exercícios Práticos

Recomendo que os exercícios práticos sejam feitos em python utilizando `@dataclasss`.

1. Calcular a área de um circulo.

    Defina uma classe para o circulo com o atributo raio.
    Defina um método para calcular a área ( `PI * raio * raio` ).

1. Calcular a área de um quadrado.

    Defina uma classe para o circulo com o atributo lado.
    Defina um método para calcular a área ( `lado * lado` ).

1. Crie métodos fabricas para as classes circulo e quadrado.

    Defina um método estático, que construa os objetos usando valores fornecidos pelo usuário, para cada classe.

## Projeto Integrador

1. Esboce diagramas [UML das classes](https://plantuml.com/class-diagram) com seus métodos

1. Debata com os demais integrantes dos grupos, se necessário atualize seus diagramas

---

## Dúvidas frequentes

- O que é o `self` no python?

    > Em Python, o self é uma convenção utilizada dentro de uma classe para se referir ao próprio objeto criado a partir dessa classe.
    > Ele é um parâmetro implícito que indica que um método pertence à instância da classe em que está sendo utilizado.
    >
    > Ao chamar um método em um objeto específico da classe, o Python automaticamente passa a própria instância como o primeiro argumento para o método, usando o parâmetro self. Isso permite que o método acesse e trabalhe com os  dados específicos daquela instância.
    >
    > Em resumo, o self é uma convenção em Python usada como o primeiro parâmetro em métodos de classe, permitindo que o método acesse os atributos e métodos da instância em que está sendo chamado. Ele representa o próprio objeto criado a partir da classe e é uma parte importante da programação orientada a objetos em Python.
    > [^chat-gpt]

    É o equivalente ao `this` em várias outras linguagens.

- O que é o `this`?

    > Em Java, a palavra-chave this é usada para se referir ao próprio objeto em que um método está sendo executado. Ela é usada para diferenciar entre os atributos do objeto e outras variáveis ​​com o mesmo nome, permitindo    acesso ao atributo específico do objeto atual.
    > [^chat-gpt]

- O que é o `-> Self`?

    O `Self` com "S" maiúsculo é uma "referência" a classe atual,
    Quando um método tem esse *type hint* significa que ele espera como parâmetro, ou retorna, o mesmo tipo da classe que ele pertence.

    <https://peps.python.org/pep-0673/>
    <https://docs.python.org/3/library/typing.html?highlight=typing#typing.Self>
