# Interfaces

> Interfaces são um conceito fundamental na programação orientada a objetos (POO) que define um contrato ou um conjunto de métodos que uma classe deve implementar. Elas representam um tipo abstrato que descreve o comportamento esperado, mas não fornece uma implementação concreta dos métodos. As interfaces servem como um meio de definir uma estrutura comum para várias classes relacionadas, permitindo a intercambiabilidade e a padronização no código.
>
> Em termos simples, uma interface especifica quais métodos uma classe deve ter, mas não como esses métodos devem ser implementados. Isso incentiva a coesão e o baixo acoplamento, permitindo que várias classes diferentes compartilhem um conjunto comum de comportamentos.
>
> As interfaces são amplamente utilizadas para alcançar a abstração, a reutilização de código e a implementação de padrões de projeto em programação orientada a objetos. Elas também são uma parte essencial de muitas linguagens de programação, como Java, C#, TypeScript e outras.
>
> Em resumo, as interfaces em programação são um mecanismo importante para definir contratos que as classes devem cumprir, promovendo a consistência e a interoperabilidade entre diferentes partes de um programa. Elas são uma ferramenta poderosa para o desenvolvimento de software modular e extensível.
> [^chat-gpt]

---

Interfaces são de longe o meu recurso favorito das linguagens de programação, elas nos ajudam a abstrair os problemas definindo quis são as etapas do nosso programa e postergam os detalhes de implementação.
Facilitam a escrita de testes e favorecem a expressividade da nossa arquitetura.

O primeiro passo para definir uma interface é entender o que nosso programa precisa fazer, mas ainda não precisamos saber o como.
Imagine que precisamos enviar a informação de um sensor para um servidor.
Podemos começar definindo quais são essas informações usando uma classe de dados e definir uma interface de comunicação com nosso servidor.

```plantuml
@startuml

enum Unit {
    Celsius
    Kelvin
    Fahrenheit
}

class Data {
    + valor: float
    + unit: Unit
}

interface Connection {
    + send(Data): void
}

Unit <.. Data
Data <.. Connection

@enduml
```

A unit de medida ( Unit) e os dados ( Data ) são classes concretas, já estão totalmente definidas inclusive com seus detalhes de implementação.
Já a conexão com o servidor ( Connection ) é um tipo abstrato ou interface, ainda não sabemos como vai funcionar,
mas sabemos que devemos conseguir enviar dados utilizando sua implementação.

Lembrando que não é possível instanciar objetos dessa interface,
só podemos criar objetos de classes concretas.
Assim, para podermos adiantar nosso lado em quanto o servidor não está pronto, supondo que outro time está responsável por isso,
podemos fazer uma implementação de "mentira" para nossa conexão.
Muitas vezes chamamos isso de "Mock" ou "Fake".

```plantuml
@startuml

enum Unit {
    Celsius
    Kelvin
    Fahrenheit
}

class Data {
    + valor: float
    + unit: Unit
}

interface Connection {
    + send(Data): void
}

class FakeConnection {
    + send(Data): void
}

note right of FakeConnection::send
    Finge enviar os dados para o servidor
end note

Unit <.. Data
Data <.. Connection
Connection <|-- FakeConnection

@enduml
```

Agora podemos usar a implementação `FakeConnection` para simular o servidor,
e escrever o restante do nosso código.
Quando o servidor finalmente for definido,
podemos escrever a implementação real.

```plantuml
@startuml

enum Unit {
    Celsius
    Kelvin
    Fahrenheit
}

class Data {
    + valor: float
    + unit: Unit
}

interface Connection {
    + send(Data): void
}

class FakeConnection {
    + send(Data): void
}

note right of FakeConnection::send
    Finge enviar os dados para o servidor
end note


class HttpConnection {
    + host: string
    + port: int
    + send(Data): void
}

note right of HttpConnection::send 
    Envia os dados via HTTP
end note

Unit <.. Data
Data <.. Connection
Connection <|-- FakeConnection
Connection <|-- HttpConnection
@enduml
```

É interessante manter a conexão de mentira no código para escrevermos testes,
mas no código de produção já podemos usar a conexão real via HTTP.

Pode ser em que algum momento a equipe decida trocar a conexão com o servidor de HTTP por MQTT,
nos forçando a trocar a implementação da conexão,
mas o restante do nosso código não precisa ser modificado,
pois ele está acoplado somente com a interface de conexão que é mais genérica/abstrata.

```plantuml
@startuml

enum Unit {
    Celsius
    Kelvin
    Fahrenheit
}

class Data {
    + valor: float
    + unit: Unit
}

interface Connection {
    + send(Data): void
}

class FakeConnection {
    + send(Data): void
}

note right of FakeConnection::send
    Finge enviar os dados para o servidor
end note


class HttpConnection {
    + host: string
    + port: int
    + send(Data): void
}

note right of HttpConnection::send 
    Envia os dados via HTTP
end note



class MqttConnection {
    + host: string
    + port: int
    + send(Data): void
}

note right of MqttConnection::send 
    Envia os dados via MQTT
end note

Unit <.. Data
Data <.. Connection
Connection <|-- FakeConnection
Connection <|-- HttpConnection
Connection <|-- MqttConnection

@enduml
```

Agora temos três opções de conexão, a de mentira, a HTTP e a MQTT.
Não precisamos remover as classes que não estamos usando no momento.
Pode ser em que algum momentos vamos querer poder deixar o usuário escolher o tipo de conexão.

Podemos usar interfaces para abstrair os sensores também.
Para nossa interface conexão é importante manter o formato dos dados,
então um sensor pode ser alguma coisa que provê dados no formato correto.
Se ainda não sabemos como o sensor vai funcionar podemos começar com um sensor de "mentira" também.

```plantuml
@startuml

enum Unit {
    Celsius
    Kelvin
    Fahrenheit
}

class Data {
    + valor: float
    + unit: unit
}

interface Sensor {
    + read() : Data
}

class ConstSensor {
    + unit: unit
    + value: float
    + read() : Data
}

note right of ConstSensor::read 
    Cria um objeto Data com valores configurados
end note

Unit <.. Data
Data <.. Sensor
Sensor <|-- ConstSensor

@enduml
```

Com essas interfaces já definidas, podemos dizer que nosso sistema é apenas uma lista de sensores que são lidos e tem seus dados enviados por uma conexão.

```plantuml
@startuml

interface Sensor {
    + read() : Data
}


interface Connection {
    + send(Data): void
}



class System {
    + sensors: Sensor[]
    + connection: Connection

    + run(): void
}


note right of System::run 
    for sensor in sensors {
        connection.send(sensor.read())
    }
end note

Sensor "0..*" --o System
Connection "1" --* System

@enduml
```

Para adicionarmos novos sensores ou novas conexões só precisamos criar novas implementações de suas interfaces, mas não precisamos modificar a classe sistema.

## Listas em Java

Como vocês já devem ter viso em estrutura de dados,
temos várias maneiras de programar uma lista,
com arrays, encadeamento simples, encadeamento duplo e etc.
Para o consumidor da lista muitas vezes esses detalhes de implementação não são importantes,
para o consumidor apenas a interface da lista é importante,
saber que ela tem a capacidade de armazenar itens.

Em java temos a interface List e diferentes implementações, como o ArrayList e o LinkedList.

```plantuml

@startuml

interface List<E> {
    + add(E item): boolean
    + clear(): void
    + get(int) : E
} 

class LinkedList<E> {
    + add(E item): boolean
    + clear(): void
    + get(int) : E
}

class ArrayList<E> {
    + add(E item): boolean
    + clear(): void
    + get(int) : E
}

List <|-- ArrayList 
List <|-- LinkedList

@enduml

```

```java

import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main {
    public static void main(String... args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        System.out.println(arrayList);

        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(1);
        System.out.println(linkedList);

        List<Integer> list;

        list = new ArrayList<>();
        list.add(1);
        System.out.println(list);


        list = new LinkedList<>();
        list.add(1);
        System.out.println(list);
    }
}

```

<https://docs.oracle.com/javase/8/docs/api/java/util/List.html>

---

## C++ Virtual

Em C++ as interfaces são chamadas de classes puramente virtuais, isso porque todos seus métodos são virtuais puros, ou seja, não tem implementação.

Também podemos ter classes abstratas, essas são classes que tem pelo menos um método virtual puro.
Veremos mais sobre isso em herança.

```cpp

#include <iostream>

class A {
public:
    /**
    * Método virtual puro
    */
    virtual void a() = 0;

    virtual ~A() {

    }
};

class B : public A {
public:
    void a() {
        std::cout << "I'm the a method from B class" << std::endl;
    }

    virtual ~B() {

    }

};

class C : public A {
public:
    void a() {
        std::cout << "I'm the a method from C class" << std::endl;
    }

    virtual ~C() {

    }

};


void a_consumer(A& a) {
    a.a();
}

int main(int argc, char** argv) {
    B b;
    a_consumer(b);
    C c;
    a_consumer(c);
}

```

A palavra reservada virtual server para que o C++ faça uma checagem dinâmica, em tempo de execução,
para saber qual método chamar.
Como o método "a" foi implementado pelas classes "B" e "C", quando temos uma referencia da classe "A", que não tem uma implementação do método "a", o C++ precisa chamar a implementação das classes "B" ou "C".
A mesma ideia se aplica no destrutor, quando vamos destruir um objeto do tipo "A" na verdade precisamos destruir um objeto do tipo "B" ou "C".

```plantuml

@startuml

class A {
    + a() : void
}

class B {
    + a() : void
}

class C {
    + a() : void
}


A <|-- B
A <|-- C

@enduml

```

O consumidor de a pode trabalhar com qualquer implementação de a,
no caso desse exemplo, com instâncias da classe B e da classe C.

---

## Duck Typing

Tudo que faz "quack" é um pato.

O Python por padrão não verifica se uma classe implementa uma interface,
seus consumidores apenas tentam usar os métodos esperados.
Um consumidor de pato espera que todo pato tenha um método "quack".

```python
class Duck:

    def quack(self):
        raise NotImplementedError()

class Mallard:

    def quack(self):
        print('Quack')

class Canvasback:

    def quack(self):
        print('Quaaack')


def do_quack(duck: Duck):
    duck.quack()


mallard = Mallard()
canvasback = Canvasback()

do_quack(mallard)
do_quack(canvasback)
```

Dessa maneira qualquer classe que implemente o método "quack" pode ser usada como um pato.

```plantuml

@startuml

class Duck {
    + quack(): None
}


class Mallard {
    + quack(): None
}

class Canvasback {
    + quack(): None
}

@enduml

```

Porém, também temos a opção de usar o pacote "abc" ( Abstract Base Class).
Esse pacote fornece alguns recursos para validar as implementações das interfaces,
que nesse caso chamamos de classes abstratas.

```python

from abc import ABC, abstractmethod

class Duck(ABC):

    @abstractmethod
    def quack(self):
        raise NotImplementedError()

class Mallard(Duck):

    def quack(self):
        print('Quack')

class Canvasback(Duck):

    def quack(self):
        print('Quaaack')


def do_quack(duck: Duck):
    duck.quack()


mallard = Mallard()
canvasback = Canvasback()

do_quack(mallard)
do_quack(canvasback)
```

```plantuml

@startuml

class Duck {
    + quack(): None
}


class Mallard {
    + quack(): None
}

class Canvasback {
    + quack(): None
}

ABC <|-- Duck
Duck <|-- Mallard
Duck <|-- Canvasback

@enduml

```

O pacote "abc" não nos permite criar objetos de classes abstratas.
Ele verifica se todos os métodos necessários foram implementados.

```python

from abc import ABC, abstractmethod

class Duck(ABC):

    @abstractmethod
    def quack(self):
        raise NotImplementedError()

class Mallard(Duck):
    pass


# An error
mallard = Mallard()

do_quack(mallard)
```

Em python e em c++ não temos a palavra reservada `interface`, ambas as linguagens suportam o que chamamos de herança múltipla, assim podemos usar classes como interfaces.

<https://pt.wikipedia.org/wiki/Duck_typing>

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios Práticos

1. Interface de Área

    - Crie uma interface comum para o método área e a implemente nas classes Retângulo, Circulo e Quadrado.
    - Crie uma função/método que receba uma lista da interface do método área e some a area de todos os elementos
    - Execute a função/método utilizando listas com valores diversos, usando combinações diferentes dos tipos Retângulo, Circulo e Quadrado.

---

## Projeto Integrador

1. Identifique possíveis interfaces no projeto integrador
1. Discuta com o grupo
1. Codifique as interfaces e suas implementações
