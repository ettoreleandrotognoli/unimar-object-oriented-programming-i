# Herança

> Herança é um dos pilares fundamentais da programação orientada a objetos (POO) e é um conceito que permite criar novas classes baseadas em classes existentes. Ela é frequentemente descrita como uma relação "é um" entre as classes. Isso significa que uma classe derivada (ou subclasse) herda características e comportamentos de uma classe base (ou superclasse), permitindo a reutilização de código e a criação de uma hierarquia de classes.
>
> A herança permite que uma classe mais específica herde atributos e métodos de uma classe mais geral, com a opção de adicionar ou substituir esses elementos conforme necessário. Isso promove a organização do código, a manutenção e a coesão, uma vez que comportamentos comuns podem ser centralizados em classes de nível superior e compartilhados por subclasses.
>
> Além disso, a herança é uma ferramenta importante para a criação de abstrações e modelagem de objetos do mundo real, refletindo relações de especialização e generalização.
>
> Em resumo, a herança é um princípio essencial da POO que permite a criação de novas classes com base em classes existentes, promovendo a reutilização de código, a organização do software e a modelagem de relações entre objetos de forma eficiente.
> [^chat-gpt]

---

Muito cuidado com herança, é um recurso poderoso mas deve ser usado com cuidado.
A herança pode quebrar encapsulamentos e tornar seu código muito estático.
O encapsulamento pode ser quebrado, pois quando fazemos herança de uma classes ganhamos acesso a suas variáveis protegidas.
O código se torna estático porque não é possível trocar a classe herdada em tempo de execução, apenas alterando o código original.

Nas linguagens que não temos interfaces, geralmente temos herança múltipla, assim podemos herdar quantas classes quiser.
Normalmente isso não é um problema quando estamos herdando apenas de classes que na verdade são interfaces.
Mas quando começamos a utilizar herança múltiplas com classes concretas podemos ter algumas complicações.
Em linguagens sem herança multiplica sempre pensamos duas vezes antes de fazer uma herança, pois não poderemos herdar mais nada depois disso.

Meu conselho é sempre evitar a herança, prefira utilizar conceitos de composição e delegação.
Dessa maneira temos o que chamamos de herança dinâmica, podemos trocar a classe herdada em tempo de execução.

Mas vamos começar com a herança estática simples.

Quando herdamos uma classe, herdamos suas características, ou seja,
temos todos os mesmos atributos e métodos.

```cpp,ocirun
#include <iostream>


class Animal {
public:
    const char* const nome;

    Animal(const char * const nome) : nome(nome) {

    }

};

class Cachorro: public Animal {
public:
    Cachorro(const char* const nome) : Animal(nome) {

    }

};

int main(int argc, char** argv) {
    Animal animal("Fuu");
    Cachorro cachorro("Bar");

    std::cout << animal.nome << std::endl;
    std::cout << cachorro.nome << std::endl;
}

```

```plantuml

@startuml

class Animal {
    + nome: string
}

class Cachorro {

}

Cachorro --|> Animal

@enduml

```

Nesse exemplo a classe `Animal` definiu o atributo `nome`,
e como a classe `Cachorro` herdou `Animal` ela também possui o atributo `nome`.
Note que é necessário definir como construir o `Animal` no  construtor da classe `Cachorro`.
Na classe `Animal` inicializamos o atributo `nome` usando o parâmetro `nome`,
e na classe `Cachorro` reutilizamos o construtor da classe `Animal`.

Além de herdar atributos, também herdamos métodos.

```cpp,ocirun
#include <iostream>


class Animal {
public:
    const char* const nome;

    Animal(const char * const nome) : nome(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Zzzzz" << std::endl;
    }

};

class Cachorro: public Animal {
public:
    Cachorro(const char* const nome) : Animal(nome) {

    }

};

int main(int argc, char** argv) {
    Animal animal("Fuu");
    Cachorro cachorro("Bar");
    animal.barulho();
    cachorro.barulho();
}

```

```plantuml

@startuml

class Animal {
    + nome: string
    + barulho(): void
}

class Cachorro {

}

Cachorro --|> Animal

@enduml

```

Nesse exemplo não precisamos criar um método barulho para a classe `Cachorro`,
ela usa o mesmo método da classe `Animal`.

## Sobrescrita

Geralmente não queremos ter exatamente o mesmo comportamento nas sub classes.
Classes bases tendem a ser mais genéricas e as sub classes tendem a ter comportamentos mais específicos.

Para isso precisamos substituir o métodos nas sub classes.

```cpp,ocirun
#include <iostream>


class Animal {
public:
    const char* const nome;

    Animal(const char * const nome) : nome(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Zzzzz" << std::endl;
    }

};

class Cachorro: public Animal {
public:
    Cachorro(const char* const nome) : Animal(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Au au" << std::endl;
    }

};


int main(int argc, char** argv) {
    Animal animal("Fuu");
    Cachorro cachorro("Bar");
    animal.barulho();
    cachorro.barulho();
}

```

Em C++ é importante tornar os métodos que podem ser substituídos em métodos virtuais.
O exemplo anterior funciona normalmente, mas se utilizarmos as ideias de interface e polimorfismo podemos ter comportamentos não esperados.

```cpp,ocirun
#include <iostream>


class Animal {
public:
    const char* const nome;

    Animal(const char * const nome) : nome(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Zzzzz" << std::endl;
    }

};

class Cachorro: public Animal {
public:
    Cachorro(const char* const nome) : Animal(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Au au" << std::endl;
    }

};

void cutucar(Animal& animal) {
    animal.barulho();
}


int main(int argc, char** argv) {
    Animal animal("Fuu");
    Cachorro cachorro("Bar");
    cutucar(animal);
    cutucar(cachorro);
}

```

Então para arrumar esse problema adicionamos o modificador `virtual` no método, assim o C++ sabe que precisa fazer
uma chamada dinâmica ao método.

```cpp,ocirun
#include <iostream>


class Animal {
public:
    const char* const nome;

    Animal(const char * const nome) : nome(nome) {

    }

    virtual void barulho() const {
        std::cout << nome << ": " << "Zzzzz" << std::endl;
    }

};

class Cachorro: public Animal {
public:
    Cachorro(const char* const nome) : Animal(nome) {

    }

    void barulho() const {
        std::cout << nome << ": " << "Au au" << std::endl;
    }

};

void cutucar(Animal& animal) {
    animal.barulho();
}


int main(int argc, char** argv) {
    Animal animal("Fuu");
    Cachorro cachorro("Bar");
    cutucar(animal);
    cutucar(cachorro);
}

```

```plantuml

@startuml

class Animal {
    + nome: string
    + barulho(): void
}

class Cachorro {
    + barulho(): void
}

Cachorro --|> Animal

@enduml

```

---

## Exercícios Práticos

1. Codifique o seguinte UML:

```plantuml

@startuml

class Movimento {
    valor: float
    descricao: string
}

class Conta {
    saldo: float
    movimentos: Movimento[]
}

Movimento ..o Conta

class Relatorio {
    # inicio()
    # fim()
    # converter(movimento: Movimento): string
    + criar(file: File, conta: Conta)
}

Relatorio ..> Conta
Relatorio ..> Movimento

note right of Relatorio::converter
    Converte um movimento para uma linha de texto
end note

note bottom of Relatorio
    inicio()
    for movimento in conta.movimentos {
        file.write(convert(movimento))
    }
    fim()
end note

class RelatorioHtml {
    # converter(movimento: Movimento): string
}

note right of RelatorioHtml::converter
    Converte um movimento para uma linha de html
end note

RelatorioHtml --|> Relatorio

@enduml

```

---

## Projeto Integrador

1. Identifique possíveis hierarquias de classes no projeto integrador
1. Discuta com o grupo
1. Codifique as classes e sub classes
