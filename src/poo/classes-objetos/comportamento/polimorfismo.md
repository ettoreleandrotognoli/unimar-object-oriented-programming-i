# Polimorfismo

> Polimorfismo é um conceito fundamental na programação orientada a objetos (POO) que se baseia na capacidade de objetos de diferentes classes responderem a uma mesma mensagem de maneira específica e apropriada a cada classe. Em outras palavras, o polimorfismo permite que diferentes objetos possam ser tratados de maneira uniforme, simplificando a interação entre eles.
>
> O termo "polimorfismo" deriva do grego, significando "muitas formas". Na POO, isso se traduz em objetos de diferentes classes compartilhando uma mesma interface ou superclasse e, assim, sendo capazes de responder de maneira única às mensagens enviadas a essa interface com base em sua própria implementação.
>
> O polimorfismo pode ser alcançado através de mecanismos como herança, interfaces e sobrescrita de métodos. Ele desempenha um papel crucial na flexibilidade e na extensibilidade do código, permitindo que novas classes sejam adicionadas sem a necessidade de alterar o código existente.
>
> Em resumo, o polimorfismo é um princípio importante da programação orientada a objetos que permite tratar objetos de diferentes classes de maneira uniforme, tornando o código mais flexível e extensível. Ele promove a reutilização de código e facilita a criação de sistemas mais complexos e dinâmicos.
> [^chat-gpt]

---

Polimorfismo é algo que estamos usando quase desde o começo das aulas.
Quando definimos as classes Quadrado, Triangulo, Retângulo e Circulo,
todas essas classes tinham uma interface em comum, o método de calcular área,
Porém cada uma dessas classes tinha uma lógica diferentes para calcular área,
ou seja, cada uma tinha um comportamento diferentes.
Essa é basicamente a definição de polimorfismo, mesma interface e diferentes comportamentos.
Isso permites que criemos softwares com comportamentos dinâmicos de maneira mais organizada,
diminuímos o uso das estruturas de controle, como `if/else`, e tentamos pensar no software como um fluxo continuo.

Espero que lembrem que em python não temos o `swicth/case`, apesar que hoje em dia temos algo semelhante o [`pattern matching`](https://peps.python.org/pep-0636/), mas vamos ignorar isso por enquanto.
Por muito tempo o único modo de fazer algo parecido com o `switch` era usando um dicionário com funções.
O que vai de encontro com a ideia de preferir polimorfismo do que estruturas de switch.

Um `switch` simples em C pode ser feito dessa maneira:

```c,ocirun
#include <stdio.h>

int main(int argc,char** argv) {
    int opcao = 0;
    switch (opcao) {
        case 0:
            printf("Opção 0\n");
            break;
        case 1:
            printf("Opção 1\n");
            break;
        default:
            printf("Opção desconhecida\n");
    }
    return 0;
}

```

Já em python precisamos usar um dicionário com nossas funções:

```python,ocirun

def opcao0():
    print("Opção 0")

def opcao1():
    print("Opção 1")

def desconhecido():
    print("Opção desconhecida")

menu = {
    0: opcao0,
    1: opcao1,
}

if __name__ == '__main__':
    opcao = 0
    acao = menu.get(opcao, desconhecido)
    acao()
```

Como não criamos nenhuma classes ou interface de forma direta, pode parecer que não estamos usando polimorfismo.
Mas em python tudo é um objeto, inclusive nossas funções.
Pensando dessa forma, podemos dizer que criamos objetos do tipo função.
Esses objetos tem a mesma interface pois podemos usá-los como funções que não recebem parâmetros,
qualquer opção escolhida resulta um objeto compatível com essa interface.

Os dois exemplos tem resultados bem semelhantes, porém o exemplo em python é mais dinâmico e simples de extender.
Podemos facilmente adicionar mais opções no dicionário, já no exemplo em C, para fazer qualquer alteração vamos precisar alterar o código do `switch`.

```python,ocirun
def opcao0():
    print("Opção 0")

def opcao1():
    print("Opção 1")

def desconhecido():
    print("Opção desconhecida")


menu = {
    0: opcao0,
    1: opcao1,
}

def adicionar_opcoes(menu:dict):
    menu[2] = lambda : print("Opção 2")
    menu[3] = lambda : print("Opção 3")

if __name__ == '__main__':
    adicionar_opcoes(menu)
    opcao = 2
    acao = menu.get(opcao, desconhecido)
    acao()

```

Vamos analisar um caso em que o polimorfismo pode ser mais importante ainda,
com o padrão de projeto [Command](https://refactoring.guru/design-patterns/command).
Um exemplo clássico desse padrão é para criar as funcionalidades de fazer e desfazer,
geralmente associados com `ctrl+y` e `ctrl+z`.
Para conseguir essa funcionalidade precisamos ter uma pilha/lista de objetos que usamos para executar ou desfazer as operações do nosso sistema.

```plantuml

@startuml

interface Command {
    + do()
    + undo()
}

note right of Command::do
    Executa a operação
end note

note right of Command::undo
    Desfaz a operação
end note

@enduml

```

A pilha de operações não precisa saber os detalhes de cada operação, apenas que elas podem ser feitas e desfeitas.
Cada operação tem comportamentos diferentes para fazer e desfazer, ou seja, polimorfismo.

Vamos usar um exemplo antigo, o da conta bancaria.

```plantuml

@startuml

class Movement {
    + value: float
    + description: float
}

class Account {
    - movements: Movement[]
    + get_balance(): float
}

Movement --o Account


@enduml

```

Vamos definir algumas operações, deposito ( `Deposit`) e saque ( `Saque`), seguindo a interface `Command`.

```plantuml


@startuml

interface Command {
    + do()
    + undo()
}


class Deposit {
    id: string
    account: Account
    value: float
    + do()
    + undo()
}

note left of Deposit::do
    Efetua o deposito
end note

note left of Deposit::undo
    Desfaz o deposito
end note

Deposit --> Command

class Withdraw {
    id: string
    account: Account
    value: float
    + do()
    + undo()
}

note left of Withdraw::do
    Efetua saque
end note

note left of Withdraw::undo
    Desfaz saque
end note


Withdraw --> Command

@enduml

```

Em python poderíamos escrever algo parecido com isso:

```python,ocirun
import uuid
from typing import List
from dataclasses import dataclass, field


def gen_id() -> str:
    return uuid.uuid4().hex

class Command:

    def do(self):
        raise NotImplementedError()

    def undo(self):
        raise NotImplementedError()

@dataclass
class Withdraw:
    id: str
    account: 'Account' = field(repr=False)
    value: float

    def do(self):
        movement = Movement(
            value=-self.value,
            description=f'Withdraw#{self.id} of {self.value:.2f}'
        )
        self.account.movements.append(movement)

    def undo(self):
        movement = Movement(
            value=+self.value,
            description=f'Undo Withdraw#{self.id} of {self.value:.2f}'
        )
        self.account.movements.append(movement)

@dataclass
class Deposit:
    id: str
    account: 'Account' = field(repr=False)
    value: float

    def do(self):
        movement = Movement(
            value=self.value,
            description=f'Deposit#{self.id} of {self.value:.2f}'
        )
        self.account.movements.append(movement)

    def undo(self):
        movement = Movement(
            value=-self.value,
            description=f'Undo Deposit#{self.id} of {self.value:.2f}'
        )
        self.account.movements.append(movement)

@dataclass
class Movement:
    value: float
    description: str

@dataclass
class Account:
    movements: List[Movement] = field(default_factory=list)

    def get_balance(self) -> float:
        return sum(map(lambda it: it.value, self.movements))


    def deposit(self, value) -> Command:
        return Deposit(
            id=gen_id(),
            account=self,
            value=value,
        )

    def withdraw(self, value) -> Command:
        return Withdraw(
            id=gen_id(),
            account=self,
            value=value,
        )

    def report(self):
        print(f"Account balance: {self.get_balance():.2f}")
        print("--------------------")
        for movement in self.movements:
            print(f"{movement.value:+.2f} - {movement.description}")
        print("--------------------")


if __name__ == '__main__':
    actions = []
    account = Account()
    deposit = account.deposit(100.)
    deposit.do()
    actions.append(deposit)
    withdraw = account.withdraw(50.)
    withdraw.do()
    actions.append(withdraw)
    account.report()
    print(actions)
    print()
    actions[-1].undo()
    actions.pop()
    account.report()
    print(actions)
    print()
    actions[-1].undo()
    actions.pop()
    account.report()
    print(actions)
```

<!--
[Chain of Responsability](https://refactoring.guru/design-patterns/chain-of-responsibility)
-->

## Exercícios de Fixação

1. Implemente uma classe `CommandStack` que controla a lista de comandos. Essa classe deve executar as ação antes de colocar elas na lista e deve desfaze-las antes de remover ( `ctrl + z`).

1. A classe `CommandStack` deve armazenar operações desfeitas para poder refaze-lás caso necessário ( `ctrl + y`).

1. Cria a operação de transferência.

## Projeto Integrador

1. Identifique casos em que o polimorfismo pode ser útil no projeto integrador

1. Implemente pelo menos um desses casos.
    Pode ser que já tenha sido implementado nos exercícios de interface.

---

[^chat-gpt]: <https://chat.openai.com/>

---
