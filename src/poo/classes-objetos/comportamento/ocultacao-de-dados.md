# Ocultação de Dados

> A ocultação de dados em programação é um conceito que se refere à prática de esconder os detalhes internos e específicos de uma classe ou objeto, de forma que somente os aspectos essenciais e seguros sejam acessíveis externamente.
>
> Vamos imaginar que uma classe é como uma caixa que pode armazenar informações. A ocultação de dados permite que a caixa tenha uma tampa, e somente algumas partes da caixa, que são seguras e essenciais, podem ser acessadas por outras partes do programa. O restante da caixa, que pode ser complexo ou sensível, fica escondido e protegido.
>
> Essa prática é importante porque:
>
> - Protege os dados
>
>    Ao ocultar os detalhes internos, evitamos que outros trechos do programa modifiquem os dados de maneira indevida, o que pode causar problemas.
>
> - Simplifica o uso
>
>   Ao expor apenas as partes essenciais e seguras da classe, tornamos o uso dessa classe mais simples e intuitivo para quem a utiliza.
>
> - Facilita a manutenção
>
>   Se precisarmos alterar a implementação interna da classe, podemos fazer isso sem afetar as outras partes do programa que a utilizam, desde que as partes essenciais continuem funcionando da mesma forma.
>
> Resumindo, a ocultação de dados em programação é como colocar uma tampa na caixa para esconder os detalhes internos complexos ou sensíveis de uma classe, permitindo que apenas as partes seguras e essenciais sejam acessadas externamente. Isso ajuda a proteger os dados, simplificar o uso e facilitar a manutenção do programa.
> [^chat-gpt]

---

Podemos ocultar dados de várias formas.
Uma das formas é utilizando modificadores de acesso, como `private` e `protected`.
Porém, com essa abordagem estamos apenas evitando que o os programadores acessem atributos ou métodos,
mas esses programadores vão criar acoplamentos com a classe concreta utilizada.
Em alguns casos ainda queremos deixar algumas propriedades públicas mas evitar sua utilização fora de certos contextos.

```plantuml

@startuml

class ConcreteClass {
    + publicMethod()
    + notSoPublicMethod()
}

class ConsumerClass {
    + method()
    + consumerMethod(obj: ConcreteClass)
}

ConsumerClass ..> ConcreteClass

@enduml

```

```java
class Main {

    static class ConcreteClass {
        public void publicMethod() {

        }

        public void notSoPublicMethod() {

        }
    }

    static class ConsumerClass {

        public void method() {
            ConcreteClass obj = new ConcreteClass();
            consumerMethod(obj);
        }

        public void consumerMethod(ConcreteClass obj) {
            obj.publicMethod();
            obj.notSoPublicMethod();
        }

    }

    public static void main(String... args) {
        ConsumerClass consumer = new ConsumerClass();
        consumer.method();
    }

}
```

```plantuml

@startuml

Main <--> ConsumerClass: "new ConsumerClass()"
activate ConsumerClass
Main --> ConsumerClass: "consumer.method()"
ConsumerClass <--> ConcreteClass: "new ConcreteClass()"
activate ConcreteClass
ConsumerClass --> ConsumerClass: "consumerMethod(obj)"
ConsumerClass <--> ConcreteClass: "obj.publicMethod()"
ConsumerClass <--> ConcreteClass: "obj.notSoPublicMethod()"

@enduml

```

A classe `ConsumerClass` tem dependências da classe `ConcreteClass`.
Temos a dependência como parâmetro no método `consumerMethod(ConcreteClass obj)`,
e temos uma dependência no corpo do método `method` quando criamos um novo objeto com `new ConcreteClass()`.
Assim, algumas modificação na classe `ConcreteClass` podem criar um problemas no nosso código, podem exigir alguma mudança.

A classe `ConsumerClass` também tem acesso ao método `notSoPublicMethod`, já que ele é público,
mas a ideia era evitar esse acesso.
Poderíamos simplesmente torna-lo privado ou protegido, mas em alguns casos isso pode ser inviável.

O dado que podemos ocultar aqui é basicamente o tipo concreto, queremos que o `ConsumerClass` acesse o `publicMethod`, mas não queremos que ele acesse ou tenha conhecimentos do tipo `ConcreteClass` e de seu método público `notSoPublicMethod`.
Então vamos criar uma interface com o que queremos dar acesso, o método `publicMethod`.
A classe `ConsumerClass` também não deve fazer referências a classe `ConcreteClass`,
então não vamos mais poder usar o `new ConcreteClass()`.

```plantuml

@startuml

class ConsumerClass {
  + method()
  + consumerMethod(obj: Interface)
}

note left of ConsumerClass::method
    Ainda temos um problema aqui
    ConcreteClass obj = new ConcreteClass();
    Interface obj = new ???????;
end note

interface Interface {
    + publicMethod()
}

class ConcreteClass {
    + publicMethod()
    + notSoPublicMethod()
}

ConcreteClass --|> Interface

ConsumerClass ..> Interface

@enduml

```

Para remover o `new ConcreteClass()` vamos usar o conceito de fábrica,
vamos usar um objeto que fabrica/constrói/instancia o objeto que precisamos.
Esse objeto vai ter acoplamento/dependência do tipo que ele constrói,
mas ele vai evitar a propagação dessa dependência usando a interface que definimos.

```plantuml

@startuml

class Factory {
    + create() : Interface
}

note left of Factory::create
    new ConcreteClass()
end note

Factory ..> Interface
Factory ..> ConcreteClass

class ConsumerClass {
  factory: Factory
  + method()
  + consumerMethod(obj: Interface)
}

note left of ConsumerClass::method
    obj = factory.create()
end note

interface Interface {
    + publicMethod()
}

class ConcreteClass {
    + publicMethod()
    + notSoPublicMethod()
}

ConcreteClass --|> Interface

ConsumerClass ..> Interface
ConsumerClass ..> Factory

@enduml

```

Agora nossa classe `ConsumerClass` depende da nossa interface e da nossa fábrica.
Ainda estamos acoplados a interface, mudanças nessa interface podem quebrar nosso código,
mas isso já é esperado em qualquer interface.
Nossa fabrica pode mudar o tipo concreto construído sem afetar nossa classe `ConsumerClass`,
pois essa classe não se importa mais com o tipo concreto desde que ele obedeça a interface definida.
A classe `ConsumerClass` não tem visibilidade do método `notSoPublicMethod`, a final,
esse método não existe na nossa interface.

Podemos mudar drasticamente o comportamento do nosso software alterando apenas a fábrica.

```plantuml

@startuml

class Factory {
    + create() : Interface
}

note left of Factory::create
    new ChangedConcreteClass()
end note

Factory ..> Interface
Factory ..> ChangedConcreteClass

class ConsumerClass {
  factory: Factory
  + method()
  + consumerMethod(obj: Interface)
}

interface Interface {
    + publicMethod()
}

class ChangedConcreteClass {
    + publicMethod()
    + changedNotSoPublicMethod()
}

note right of ChangedConcreteClass
    Mudanças nessa classe não foram
    propagadas para o resto do sistema
    apenas para a fábrica
end note

ChangedConcreteClass --|> Interface

ConsumerClass ..> Interface
ConsumerClass ..> Factory

@enduml

```

Em fim, podemos ocultar dados com interfaces invés de usar modificadores de acesso.
Existem vantagens e desvantagens, usando interfaces provavelmente vamos ter que escrever mais código,
mas já estamos preparando nossa arquitetura para futuras mudanças.
Porém, prever futuras mudanças é difícil, cuidado para não adicionar complexidade desnecessária.

---

[^chat-gpt]: <https://chat.openai.com/>
