class Teste:

    def __init__(self) -> None:
        self.publico = True
        self._protegido = True
        self.__privado = True

    def get_privado(self):
        return self.__privado


test = Teste()

print(test.publico)
print(test._protegido)
print(test._Teste__privado)
print(test.get_privado())
test._Teste__privado = False
print(test.get_privado())
