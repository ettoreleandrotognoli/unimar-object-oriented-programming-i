from datetime import datetime
from dataclasses import dataclass, field, replace
from typing import Sequence, Self, List


@dataclass
class Pessoa:
    nome: str


@dataclass
class Conta:
    _titular: Pessoa
    _saldo: float = 0.
    _movimentos: List['Movimento'] = field(default_factory=list)

    @classmethod
    def create(cls, titular: Pessoa) -> Self:
        return cls(_titular=titular)

    def get_titular(self) -> Pessoa:
        return self._titular

    def get_saldo(self) -> float:
        return self._saldo

    def get_movimentos(self) -> Sequence['Movimento']:
        copia_segura = []
        for movimento in self._movimentos:
            copia_segura.append(replace(movimento))
        return copia_segura

    def add_movimento(self, movimento: 'Movimento') -> Self:
        movimento = replace(movimento, origem=self, data=datetime.now())
        self._movimentos.append(movimento)
        self._saldo += movimento.valor
        return self


@dataclass
class Movimento:
    descricao: str
    valor: float
    origem: Conta = None
    data: datetime = None


titular = Pessoa('')
conta = Conta.create(titular)
print(conta)

movimento = Movimento(
    descricao='Loteria',
    valor=1_000_000.,
)

conta.add_movimento(movimento)

print(conta)
