#include <string>
#include <ctime>
#include <list>
#include <iostream>

class Pessoa;
class Movimento;
class Conta;

class Pessoa {
public:
    std::string nome;

    Pessoa(const char * const nome) : nome(nome) {

    }
};

class Movimento {
public:
    float valor;
    Conta* conta;
    std::string descricao;
    std::time_t data;

    Movimento(float valor, const char* const descricao) :
        valor(valor),
        descricao(descricao) {
        this->conta = NULL;
    }
};

class Conta {
private:
    const Pessoa& titular;
    float saldo;
    std::list<Movimento> movimentos;
public:

    Conta(const Pessoa& titular) :
        titular(titular),
        saldo(0.) {

    }

    const Pessoa& getTitular() const {
        return this->titular;
    }

    float getSaldo() const {
        return this-> saldo;
    }

    const std::list<Movimento>& getMovimentos() const {
        return this->movimentos;
    }

    Conta& addMovimento(Movimento movimento) {
        movimento.conta = this;
        movimento.data = std::time(0);
        this->saldo += movimento.valor;
        this->movimentos.push_back(movimento);
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& output, const Conta& conta) {
        output << "Conta {";
        output << "titular: \"" << conta.titular.nome << "\", ";
        output << "saldo: " << conta.saldo << ", ";
        output << "movimentos: [";
        for(const Movimento& movimento: conta.movimentos) {
            output << "{ descricao \"" << movimento.descricao << "\", ";
            output << "valor: " << movimento.valor << ", ";
            output << "data: " << movimento.data;
            output << "} ";
        }
        output << "]}";
        return output;
    }

};

int main(int argc, char** argv) {
    Pessoa titular("");
    Conta conta(titular);
    std::cout << conta << std::endl;
    Movimento movimento(1000000, "Loteria");
    conta.addMovimento(movimento);
    std::cout << conta << std::endl;
}
