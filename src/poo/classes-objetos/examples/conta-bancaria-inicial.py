from datetime import datetime
from dataclasses import dataclass, field
from typing import List


@dataclass
class Pessoa:
    nome: str


@dataclass
class Conta:
    titular: Pessoa
    saldo: float = 0.
    movimentos: List['Movimento'] = field(default_factory=list)


@dataclass
class Movimento:
    origem: Conta
    descricao: str
    valor: float
    data: datetime = field(default_factory=datetime.now)


titular = Pessoa('')
conta = Conta(titular=titular)
print(conta)

conta.saldo += 1_000_000.
movimento = Movimento(
    origem=conta,
    valor=1_000_000.,
    descricao='Loteria',
)
conta.movimentos.append(movimento)

print(conta)
