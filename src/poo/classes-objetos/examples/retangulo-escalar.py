from dataclasses import dataclass
from typing import Self


@dataclass
class Retangulo:
    largura: float
    comprimento: float

    def calcular_area(self):
        return self.largura * self.comprimento

    def escalar(self, valor: float) -> Self:
        self.largura *= valor
        self.comprimento *= valor
        return self

    def escalado(self, valor: float) -> Self:
        return Retangulo(
            largura=self.largura*valor,
            comprimento=self.comprimento*valor
        )
