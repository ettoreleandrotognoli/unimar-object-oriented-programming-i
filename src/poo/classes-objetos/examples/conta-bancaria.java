import java.util.List;
import java.util.stream.Stream;
import java.util.ArrayList;
import java.time.ZonedDateTime;

public class Main {

    static class Pessoa {
        public String nome;

        public Pessoa(String nome) {
            this.nome = nome;
        }
    }

    static class Conta {
        private Pessoa titular;
        private float saldo;
        private List<Movimento> movimentos;

        public Conta(Pessoa titular) {
            this.titular  = titular;
            this.saldo = 0;
            this.movimentos = new ArrayList<>();
        }

        public float getSaldo() {
            return this.saldo;
        }

        public Pessoa getTitular() {
            return this.titular;
        }

        public Stream<Movimento> getMovimentos() {
            return this.movimentos.stream().map(Movimento::clone);
        }

        public Conta addMovimento(Movimento template) {
            Movimento movimento = template.clone();
            movimento.conta = this;
            movimento.data = ZonedDateTime.now();
            this.movimentos.add(movimento);
            this.saldo += movimento.valor;
            return this;
        }

        @Override
        public String toString() {
            return String.format(
                "{ titular : %s, saldo: %.2f, movimentos: %s}",
                titular.nome, saldo, movimentos
            );
        }
    }

    static class Movimento {
        public Conta conta;
        public String descricao;
        public float valor;
        public ZonedDateTime data;

        public Movimento(
            Conta conta,
            String descricao,
            float valor,
            ZonedDateTime data
        ) {
            this.conta = conta;
            this.descricao = descricao;
            this.valor = valor;
            this.data = data;
        }

        public Movimento(String descricao, float valor) {
            this.descricao = descricao;
            this.valor = valor;
        }

        @Override
        public Movimento clone() {
            return new Movimento(
                conta, descricao, valor, data
            );
        }

        @Override
        public String toString() {
            return String.format(
                "{ descricao: %s, valor: %.2f, data: %s}",
                descricao, valor, data
            );
        }

    }


    public static void main(String... args) {
        Conta conta = new Conta(new Pessoa(""));
        System.out.println(conta);
        Movimento movimento = new Movimento("Loteria", 1_000_000f);
        conta.addMovimento(movimento);
        System.out.println(conta);
    }

}
