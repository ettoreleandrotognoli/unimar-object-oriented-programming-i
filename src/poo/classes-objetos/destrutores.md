# Método Destrutor

> Na programação orientada a objetos, os métodos destrutores são usados para executar ações especiais antes que um objeto seja destruído ou removido da memória. Eles oferecem uma oportunidade de liberar recursos, encerrar conexões ou realizar outras tarefas necessárias antes que o objeto deixe de existir.
>
> Pense em um evento de encerramento em uma festa. Antes que a festa termine, é comum agradecer aos convidados, desligar as luzes e garantir que tudo esteja organizado. O método destrutor é como esse evento de encerramento para um objeto na programação. Ele permite que o objeto conclua tarefas finais antes de ser "destruído".
>
> Os métodos destrutores são úteis quando um objeto precisa liberar recursos que ocupou durante sua existência, como fechar arquivos abertos, desconectar de bancos de dados ou realizar outras tarefas de limpeza.
>
> Em muitas linguagens de programação, como C++ e Python, os métodos destrutores são chamados automaticamente quando um objeto não é mais necessário e está prestes a ser removido da memória. Isso garante que o programa não deixe "restos" indesejados ou recursos inutilizados.
>
> Portanto, os métodos destrutores são uma maneira de garantir que os objetos sejam encerrados corretamente e que quaisquer tarefas de limpeza ou liberação de recursos sejam realizadas antes que o objeto seja removido da memória.
> [^chat-gpt]

---

O método destrutor é utilizado quando a memória vai ser liberada, quando deixamos de usar a variável.
Em linguagens com [coletor de lixo](https://pt.wikipedia.org/wiki/Coletor_de_lixo_(inform%C3%A1tica)) geralmente temos um contador de referências implícito
e quando não temos mais nenhuma referência para o objeto o mesmo é liberado da memória.
Esse é o caso do Python, Java e PHP, já com C++ o gerenciamento de memória é parcialmente responsabilidade do programador, então muitas vezes temos que liberar a memória manualmente.
Nos dois casos os métodos destrutores são chamados, mas em linguagens com coletor de lixo pode ser mais complicado saber exatamente quando isso vai acontecer.

O nome do método destrutor pode variar de linguagem para linguagem,
em Python temos o [`__del__`](https://docs.python.org/3/reference/datamodel.html#object.__del__),
em PHP o [`__destruct`](https://www.php.net/manual/en/language.oop5.decon.php),
em Java o [`finalize`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#finalize--)
e em C++ o [`~class-name`](https://en.cppreference.com/w/cpp/language/destructor).

## Python

```python
class Retangulo:

    largura: float
    comprimento: float

    def __del__(self):
        pass
```

## Java

```java
class Retangulo {

    public float largura;
    public float comprimento;

    protected void finalize() throws Throwable {

    }
}
```

O método `finalize` foi descontinuado a partir do java 9,
a chamada desse método nunca foi muito estável,
já que em java não podemos prever quando o coletor de lixo será executado.

## PHP

```php
class Retangulo {

    public float $largura;
    public float $comprimento;

    function __destruct() {

    }
}
```

## C++

Na a maioria das vezes, em C++, os método destrutores precisam ser "virtuais".
Vamos clarificar isso quando entrarmos no assunto de herança e polimorfismo.

```cpp
class Retangulo {
public:
    float largura;
    float comprimento;

    virtual ~Retangulo() {

    }
}
```

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Dúvidas frequentes

- Por quê `protected`?

    O `protected` é um **modificador de acesso**, significa que somente objetos da mesma classe ou parentes tem acesso a esse atributo.
    Modificadores de acesso serão abordados mais adiante.
