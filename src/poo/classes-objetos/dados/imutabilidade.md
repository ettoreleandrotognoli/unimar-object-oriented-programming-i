# Imutabilidade

> A imutabilidade é um conceito importante em programação que se refere à característica de um objeto ou dado ser incapaz de ser alterado após sua criação. Isso significa que, uma vez que um objeto é criado, seu estado interno não pode ser modificado. Em vez disso, se houver a necessidade de fazer uma alteração, um novo objeto é criado com os valores atualizados.
>
> Vamos explorar suas vantagens:
>
> 1. Segurança e Confiabilidade: Objetos imutáveis ​​são mais seguros, pois não podem ser acidentalmente alterados por diferentes partes do código. Isso ajuda a evitar e reduzir erros e bugs inesperados no programa.
>
> 1. Fácil Testabilidade: Por serem previsíveis e não terem efeitos colaterais, objetos imutáveis ​​são mais fáceis de testar. Você pode confiar que seu comportamento será consistente em qualquer contexto.
>
> 1. Thread Safety: Objetos imutáveis ​​são intrinsecamente seguros para uso em ambientes concorrentes e threads, já que não podem ser alterados simultaneamente por múltiplas threads.
>
> 1. Melhor Desempenho: Em algumas situações, a imutabilidade pode melhorar o desempenho, pois não é necessário fazer cópias profundas dos objetos para evitar modificações indesejadas.
>
> 1. Facilita o Rastreamento de Mudanças: Com objetos imutáveis, é mais fácil rastrear alterações, pois cada modificação cria um novo objeto, permitindo acompanhar o histórico de estados.
>
> 1. Reusabilidade e Compartilhamento: Como os objetos imutáveis ​​não mudam, eles podem ser compartilhados com segurança entre diferentes partes do código, evitando duplicação e melhorando a eficiência.
>
> 1. Sem Efeitos Colaterais: A imutabilidade ajuda a evitar efeitos colaterais indesejados, tornando o código mais fácil de entender e prever o comportamento.
>
> Em linguagens funcionais, a imutabilidade é uma prática comum e é amplamente adotada para criar código mais seguro e robusto. No entanto, mesmo em linguagens orientadas a objetos, adotar a imutabilidade sempre que possível pode trazer benefícios significativos para a qualidade e manutenção do código.
> [^chat-gpt]

---

Objetos imutáveis são objetos que não mudam, tem valores constantes.
Existem diversas aplicações para esses tipos de objetos, geralmente envolvendo desempenho, programação paralela e evitar efeitos colaterais.

## Exemplo de Efeito Colateral

Em python é sempre recomendado a utilização de objetos imutáveis nos parâmetros com valor padrão,
isso evita efeitos colaterais indesejados.

No exemplo abaixo a função tem um parâmetro padrão que é uma lista vazia.
Listas em python não são imutáveis, ou seja, a função pode acabar alterando o valor padrão.
O valor padrão é um objeto que será compartilhado em todas as chamadas da função.

```python
def funcao(a=[]):
    a.append(1)
    print(a)

funcao()
funcao()
funcao()

```

Nesse exemplo a cada chamada de função o valor `1` foi adicionado na lista,
assim a partir da segunda chamada o valor padrão deixou de ser uma lista vazia.
O mesmo pode acontecer com dicionários ou qualquer outro objeto mutável.

Uma correção possível seria transformar o valor default do parâmetro `a` em uma tupla:

```python
def funcao(a=()):
    #a.append(1)
    print(a)

funcao()
funcao()
funcao()
```

Como as tuplas são imutáveis não poderíamos altera-la,
o método `append` nem existe.

---

## Domain Driven Design

> **Domain-Driven Design (DDD)**, ou "Projeto Orientado ao Domínio" em português, é uma abordagem de design de software que coloca o foco na compreensão profunda e na modelagem do domínio do problema em questão. Ela foi desenvolvida para enfrentar desafios comuns em projetos de software complexos, nos quais a comunicação eficaz entre desenvolvedores e especialistas no domínio do problema é essencial.
>
> Em outras palavras, DDD trata de criar um modelo de negócios que reflita com precisão o mundo real e que seja compreensível tanto pelos desenvolvedores quanto pelos especialistas no domínio. Isso ajuda a garantir que o software desenvolvido atenda às necessidades reais do negócio.
>
> No centro do DDD está o conceito de "domínio", que se refere ao conjunto de conhecimentos, regras e processos que compõem um determinado problema. Para desenvolver software usando DDD, os desenvolvedores trabalham em estreita colaboração com especialistas do domínio para identificar e modelar esses conceitos e regras de forma clara e precisa.
>
> Além disso, o DDD promove a criação de um "modelo de domínio" que é uma representação do problema em código. Esse modelo é organizado em "agregados", "entidades", "objetos de valor" e outros conceitos específicos do DDD, que ajudam a estruturar o código de acordo com a complexidade do domínio.
>
> O objetivo final do Domain-Driven Design é criar software que seja mais flexível, adaptável e alinhado com as reais necessidades do negócio. Isso é alcançado por meio de uma modelagem cuidadosa do domínio, da linguagem ubíqua que promove a comunicação eficaz entre todos os envolvidos no projeto e de uma arquitetura de software que reflete essa compreensão profunda do problema.
>
> Em resumo, o Domain-Driven Design (DDD) é uma abordagem de design de software que prioriza a modelagem precisa e a compreensão profunda do domínio do problema para criar software que atenda efetivamente às necessidades do negócio.
> [^chat-gpt]

---

Os "objetos de valor" do DDD são objetos imutáveis

## Dicas para Implementação

Não devemos ter métodos de alteração, ou se tivermos,
estes devem sempre retornar uma cópia modificada,
mantendo o estado objeto original.

Utilize [métodos fabricas](https://refactoring.guru/design-patterns/factory-method) e/ou [builders](https://refactoring.guru/design-patterns/builder) quando seus objetos tiverem muitos atributos.

Valores muito comuns podem/devem ser constantes, evitando copias desnecessárias na memoria.
Algum tipo numérico, por exemplo, poderia ter os valores `ZERO`, `UM` e `INFINITO` como constantes.

---

[^chat-gpt]: <https://chat.openai.com/>

<https://martinfowler.com/bliki/ValueObject.html>  
<https://carlosschults.net/pt/value-objects-ferramenta/>  

---

## Exercícios Práticos

1. Refaça o exercício de modificadores de acesso e propriedades tornando a classe imutável

      > Classe para horário
      >
      > Defina uma classe para representar horas, minutos e segundos.  
      > O atributo horas deve sempre estar entre 0 e 23.  
      > O atributo minutos deve sempre estar entre 0 e 59.  
      > O atributo segundos deve sempre estar entre 0 e 59.  
      > Crie métodos para modificar o horário de maneira segura.  
      > Crie propriedades para horas, minutos e segundos.

## Projeto Integrador

1. Identifique alguns dados do projeto integrador que podem ser representados com classes imutáveis

1. Implemente pelos um desses dados com classes imutáveis
