# Classes de Dados

— Explique para uma criança o que são classes de dados em programação orientada a objetos

> Claro! Vou explicar o que são classes de dados em programação orientada a objetos de uma forma simples.
>
> Imagine que você tem uma caixa de tesouros onde guarda coisas especiais, como joias, moedas e pedras preciosas. Cada tesouro tem características únicas, como cor, tamanho e brilho.
>
> Em programação orientada a objetos, uma classe de dados é como um modelo que define o tipo de informação que queremos guardar. É como uma "etiqueta" que diz quais características um objeto desse tipo deve ter.
>
> Por exemplo, se tivéssemos uma classe de dados chamada "Tesouro", poderíamos definir que cada tesouro precisa ter uma cor, um tamanho e um brilho.
>
> Quando criamos um objeto a partir dessa classe, podemos preencher as características específicas do tesouro. Por exemplo, podemos criar um objeto chamado "meuTesouro" e definir que ele é um tesouro de cor dourada, tamanho pequeno e brilho intenso.
>
> As classes de dados nos ajudam a organizar informações de forma estruturada. Podemos criar diferentes objetos do mesmo tipo, mas com características únicas.
>
> Voltando à nossa caixa de tesouros, podemos ter diferentes tesouros dentro dela, como um tesouro vermelho, grande e brilhante, ou um tesouro azul, pequeno e fosco. Cada um desses tesouros é um objeto único, mas todos seguem a estrutura definida pela classe de dados "Tesouro".
>
> As classes de dados são usadas para representar informações e nos ajudam a criar programas mais organizados e flexíveis. Elas nos permitem definir quais características um objeto deve ter e criar objetos com essas características.
>
> Em resumo, uma classe de dados em programação orientada a objetos é como um modelo que define o tipo de informação que queremos guardar. É como uma "etiqueta" que diz quais características um objeto desse tipo deve ter. Podemos criar objetos únicos com base nessa estrutura e preencher suas características específicas. É uma forma de organizar informações de forma estruturada e criar programas mais flexíveis.
> [^chat-gpt]

---

## Representação de dados

- Dados que trabalham juntos devem permanecer juntos.
- Assim evitamos funções com muitos parâmetros.
- Facilita a conversa entre programadores.

Imagine a situação em que não exista a palavra "liquidificador", o objeto existe, mas ninguém deu um nome para ele.
Como seria descrito em uma receita o uso do liquidificador? Como seria feita uma referencia a esse objeto?
Imagino que seria necessário descrever o formato e partes do liquidificador a todo momento, e isso não seria muito prático.
A existência de nomes para os objetos facilita muito a comunicação.

Basicamente todos nós sabemos que quando estamos falando de uma data, estamos falando de dia, mês e ano.
Inclusive o tipo de dado para representar data na maioria das linguagens de programação inclui uma maneira de acessar os valores de dia, mês e ano.

```python
from datetime import date

hoje = date.today()
print(f'{hoje.day}/{hoje.month}/{hoje.year}')
```

Neste exemplo utilizamos o tipo `date` nativo do python,
criamos um novo objeto desse tipo com a função `today`,
que retorna o objeto com a data de hoje.
Em seguida escrevemos na tela a data no formato `dia/mês/ano`, utilizando os atributos `day`, `month` e `year`.

```python
from dataclasses import dataclass

@dataclass
class Data:
    dia: int
    mes: int
    ano: int

data = Data(23, 6, 2020)
print(f'{data.dia}/{data.mes}/{data.ano}')
```

Objetos do mundo real podem ser representados de maneiras diferentes conforme o domínio do nosso problema.
Precisamos de informações diferentes sobre um cachorro para um sistema veterinário do que precisaríamos para um jogo.
Em uma clinica veterinária precisamos ter informações sobre o tutor, vacinação, idade, peso, histórico de consultas entre outras,
já em um jogo precisamos do modelo 3D do cachorro para podermos renderiza-lo.

## Dependência Explicita

Classes de dados tem algumas semelhanças com dicionários ou mapas, a grande diferença é que nos dicionários os "atributos" são controlados de forma dinâmica, já nas classes de forma estática
Ou seja, com dicionários não temos garantia de que a chave que estamos acessando exista e com classes sim.
No caso das classes, como os atributos já foram definidos de forma estática, geralmente as [IDEs](https://pt.wikipedia.org/wiki/Ambiente_de_desenvolvimento_integrado) conseguem no auxiliar melhor.

```python
def print_nome(obj: dict):
    print(obj['nome'])

objeto = {
    'nome': 'Fulano'
}

print_nome(objeto)
```

Neste exemplo estamos confiantes em usar a chave `nome` pois acabamos de definir a variável `objeto`.
Porém, muitas vezes a variável `objeto` pode não conter essa chave e nosso código iria falhar com um `KeyError: 'nome'`.
Outros programadores que utilizam essa função podem não ter certeza de que o dicionário utilizado como parâmetro precisa conter a chave `nome`.
Podemos dizer que nossa função `print_nome` tem uma dependência da chave `nome`, mas esse dependência não está explicita.

Então para resolver esses problemas poderíamos criar o nosso próprio tipo de dados, que deixa explicito a necessidade pela "chave" `nome`.
Dessa forma ao implementar a função poderíamos utilizar tranquilamente a "chave" `nome`, e outros programadores teriam certeza da necessidade desta chave.

```python
from dataclasses import dalaclass

@dataclass
class Nomeado:
    nome: str

def print_nome(nomeado: Nomeado):
    print(nomeado.nome)

objeto = Nomeado('Fulano')

print_nome(objeto)
```

Neste exemplo foi criada a classe `Nomeado`, que foi decorado por [`dataclass`](https://docs.python.org/3/library/dataclasses.html#dataclasses.dataclass), contendo o atributo `nome` do tipo `str`.

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Dúvidas frequentes
