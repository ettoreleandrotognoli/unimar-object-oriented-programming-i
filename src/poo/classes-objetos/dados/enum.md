# Enum

> Enums (Enumerations) são uma estrutura de dados em programação que nos permite representar conjuntos fixos e predefinidos de valores constantes. Eles são frequentemente usados para representar categorias, estados ou opções que não devem ser alteradas ao longo da execução de um programa.
>
> Imagine que você está escrevendo um programa para lidar com os dias da semana. Os dias da semana são um conjunto fixo de valores: segunda-feira, terça-feira, quarta-feira, e assim por diante. Um enum permite que você defina esses valores de maneira clara e legível no código.
>
> Em muitas linguagens de programação, como Java, C++, C# e Python, os enums são definidos usando palavras-chave especiais, como enum ou enum class.
>
> Os enums são úteis em situações em que você deseja limitar as opções válidas para uma variável ou parâmetro, evitando assim erros de programação. Além disso, eles tornam o código mais legível, pois os valores têm nomes descritivos em vez de números mágicos.
>
> Em resumo, Enums são uma ferramenta valiosa na programação que permite representar conjuntos predefinidos de valores constantes de maneira clara e legível. Eles são amplamente usados para melhorar a semântica do código e garantir a consistência de valores em um programa.
> [^chat-gpt]

---

Muitas vezes utilizamos constantes em nosso código para representar valares comuns, ou os valores aceitos em funções, métodos ou objetos.
Quando usamos um enum para representar esses valores a linguagem de programação ajuda a garantir a integridade dos dados, pois limita a escolha entre as contantes definidas.

Um exemplo muito comum é com os dias das semanas, é um conjunto limitado e muito bem definido,
dificilmente teremos alguma mudança nessas constantes.

Poderíamos representar os dias das semanas com strings ou com inteiros facilmente.

Exemplo de representação com inteiros:

```java
public class Day {
    public static final int SUNDAY = 0;
    public static final int MONDAY = 1;
    public static final int TUESDAY = 2;
    public static final int WEDNESDAY = 3;
    public static final int THURSDAY = 4;
    public static final int FRIDAY = 5;
    public static final int SATURDAY = 6;
}
```

Também podemos representar com strings, como nesse exemplo:

```java
public class Day {
    public static final String SUNDAY = "sunday";
    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
}
```

Em ambos o caso facilitamos o uso, podemos apenas escrever `Day.` e contar com o auto completar para escolher um dia válido.
Mas ainda não temos uma checagem eficaz ao utilizarmos uma função, método ou atributo.

Ao criar uma classe o atributo precisar ser um inteiro:

```java
public class Date {
    private int dayOfWeek;

    public Date(int dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}

```

Ou uma string:

```java
public class Date {
    private String dayOfWeek;

    public Date(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
```

Nesses dois casos podemos criar um novo objeto com:

```java
new Date(Day.SUNDAY);
```

Em ambos os casos restringimos o tipo, int o string,
mas ainda podemos qualquer usar qualquer valor de int ou string.
Ou seja, infelizmente por estarmos usando um inteiro ou uma string também poderíamos escrever algo assim:

```java
new Date(0);
new Date("sunday");
```

Ou pior, algo assim:

```java
new Date(-12312312333);
new Date("unknown");
```

Ao criarmos a classe `Day` com todas as contantes possíveis facilitamos o uso,
mas não bloqueamos o uso indevido, ou seja, ainda podemos usar dados inválidos.

Podemos então criar um enum, o enum será um novo tipo com um conjunto limitados de valores.

```java
public enum Day {
    SUNDAY,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY 
}
```

```java
public class Date {
    private Day dayOfWeek;

    public Date(Day dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }
}
```

Agora para criar um novo objeto do tipo `Date` somos forçados a usar um objeto do tipo `Day`, nosso enum.

```java
new Date(Day.SUNDAY);
```

<https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html>

---

Em python podemos criar novas classes que estendem/herdam a classe `Enum`,
tendo uma idea semelhante.
Também podemos atribuir valores para cada um dos itens.

```python
class Color(Enum):
    RED = 1
    GREEN = 2
    BLUE = 3
```

Porém, como não compilamos o python, não vamos ter as mesmas validações que teríamos em java.

<https://docs.python.org/3/library/enum.html>  
<https://docs.python.org/3/howto/enum.html>

---

## Exemplo em PHP

```php
<?php
enum Suit
{
    case Hearts;
    case Diamonds;
    case Clubs;
    case Spades;
}

```

<https://www.php.net/manual/en/language.types.enumerations.php>

## Exemplo em C++

```cpp
enum class Color {
    red,
    green = 20,
    blue
};
```

<https://en.cppreference.com/w/cpp/language/enum>

---

## UML

```plantuml
enum TimeUnit {
    DAYS
    HOURS
    MICROSECONDS
    MILLISECONDS
    MINUTES
    NANOSECONDS
    SECONDS
}

```

<https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/TimeUnit.html>

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Projeto Integrador

1. Identifique alguns dados do projeto integrador que podem ser representado por enums.

1. Implemente pelos menos um desses dados utilizando enums
