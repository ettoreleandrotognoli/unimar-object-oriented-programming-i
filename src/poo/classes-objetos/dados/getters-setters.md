# Getter & Setters

> Getters e Setters são métodos especiais em programação usados para acessar (obter) e modificar (definir) os valores dos atributos de um objeto de uma classe. Eles são comumente usados para controlar o acesso aos atributos, garantindo que os valores sejam lidos e modificados de maneira controlada.
>
> Getters são métodos que permitem obter o valor de um atributo privado de um objeto. Eles são usados para acessar informações de um objeto sem permitir acesso direto ao atributo subjacente. Os getters geralmente têm nomes descritivos que indicam qual atributo está sendo acessado.
>
> Setters, por outro lado, são métodos que permitem definir o valor de um atributo privado de um objeto. Eles são usados para modificar o estado de um objeto de acordo com regras específicas definidas na classe. Os setters também ajudam a garantir a validade dos dados atribuídos ao objeto.
>
> A utilização de getters e setters fornece controle sobre como os atributos de uma classe são acessados e alterados. Isso permite adicionar validações, restrições e lógica personalizada sempre que um atributo é lido ou modificado, garantindo a integridade dos dados e facilitando a manutenção do código.
>
> Em resumo, getters e setters são métodos usados para acessar e modificar atributos de objetos em programação. Eles ajudam a manter o encapsulamento, controlar o acesso aos atributos e aplicar regras específicas quando os valores são obtidos ou definidos.
> [^chat-gpt]

---

Em algumas linguagens temos a convenção de criar os métodos getters e/ou setters para nossas classes,
esses métodos fornecem um modo seguro de acessar os atributos.
Seguro no sentido de evitar que um programador utilize a classe de forma não planejada.
Assim, quando queremos ler um atributo usamos seu método `get` e quando queremos escrever em um atributos usamos seu método `set`.
Geralmente quando o atributo é do tipo booliano usamos `is` no lugar do `get`.
E para evitar o acesso direto aos atributos, usamos algum modificador de acesso como `protected` ou `private`.

Uma linguagem que tem essa convenção muito forte é o java, classes que seguem essa convenção geralmente são chamadas de [POJO](https://en.wikipedia.org/wiki/Plain_old_Java_object) ( Plain Old Java Object ).

```java

public class Pessoa {
    private String nome;
    private int idade;
    private boolean ehAdulto;

    public Pessoa(String nome, int idade, boolean ehAdulto) {
        this.nome = nome;
        this.idade = idade;
        this.ehAdulto = ehAdulto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isEhAdulto() {
        return ehAdulto;
    }

    public void setEhAdulto(boolean ehAdulto) {
        this.ehAdulto = ehAdulto;
    }
}

```

O uso dessa convenção é tão popular que temos diversas ferramentas para gerar esse código automaticamente.
Na minha opinião, a melhor maneira de fazer isso em java é com o [`lombok`](https://projectlombok.org/).

```java
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
public class Pessoa {
    private String nome;
    private int idade;
    private boolean ehAdulto;
}
```

Utilizando as anotações `@AllArgsConstructor`, `@Getter` e `@Setter` todos os métodos getters, setters e o construtor com todos os atributos como parâmetros serão gerados em tempo de compilação.
Assim mesmo se alteramos algum atributo, não precisamos nos preocupar em reescrever código.
Outra maneira de não se preocupar muito com isso é utilizando a linguagem [kotlin](https://kotlinlang.org/), normalmente o código gerado para a JVM já contem os getters e setters.

Essa convenção pode ser usada em outras linguagens, mas normalmente não se tem muitas vantagens em linguagens interpretadas.
Imagino que no momento seja difícil até de entender que vantagem temos em fazer isso com java,
mas espero que isso fiquei mais claro quando falarmos de propriedades, imutabilidade e interfaces.
Por enquanto imagine a situação em que um método get realiza um cálculo e não acesse um atributo.
Seus consumidores não se importam com isso, eles sempre usam o método get, independentemente se é um atributo ou um cálculo.
Se em algum momento o cálculo mudar, ou transformarmos o valor em um atributo, os consumidores não vão ser impactados,
pois a forma de acesso ainda é a mesma, pelo método get.

Usando a classe `Pessoa` como exemplo, podemos transformar o método `ìsEhAdulto` em um cálculo.

```java

public class Pessoa {
    private String nome;
    private int idade;

    public Pessoa(String nome, int idade) {
        this.nome = nome;
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isEhAdulto() {
        return this.idade >= 18;
    }
}

```

Em PHP temos o termo POPO (Plain Old PHP Object), em C++ POCO ( Plain Old C++ Object) e imagino que mais termos semelhantes em outras linguagens.
No caso de C++ temos um detalhe interessante na implementação, a palavra reservada `const` logo após os métodos de leitura.

```cpp
#include <string>

class Pessoa {
private:
    std::string nome;
    int idade;
    bool ehAdulto;

public:
    Pessoa(std::string nome, int idade, bool ehAdulto) :
        nome(nome),
        idade(idade),
        ehAdulto(ehAdulto) {}

    std::string getNome() const {
        return nome;
    }

    void setNome(std::string nome) {
        this->nome = nome;
    }

    int getIdade() const {
        return idade;
    }

    void setIdade(int idade) {
        this->idade = idade;
    }

    bool isEhAdulto() const {
        return ehAdulto;
    }

    void setEhAdulto(bool ehAdulto) {
        this->ehAdulto = ehAdulto;
    }
};
```

A palavra reservada `const` serve para deixar explicito ao compilador que esse método não modifica o estado atual do objeto,
ou seja, é um método somente de leitura.
Assim o compilador tem informações o suficiente para verificar o uso correto do método, permitindo o uso desses métodos em variáveis `const` / imutáveis.
Qualquer método sem o `const` não pode ser usado quando a variável é `const` e métodos com `const` também não podem alterar atributos internos.

Em python temos um recurso que transforma métodos em propriedades, basicamente os métodos são acessados como se fossem atributos por seus consumidores.
Acredito que por esse motivo não existe tanta preocupação com esse tipo de convenção, veremos mais sobre isso quando falarmos de propriedades.

## Comparação

Quando estamos representado dados com esses tipos de classes frequentemente queremos comparar nossos objetos.
Algumas linguagens nos permitem alterar o funcionamento da operações de comparação como o `==`.
Em java não temos essa opção, mas temos o método [`equals`](https://docs.oracle.com/javase/8/docs/api/java/lang/Object.html#equals-java.lang.Object-).
Como nosso objeto pode ser comparado com qualquer outro tipo de objeto é interessante o uso do [instanceof](https://www.baeldung.com/java-instanceof).

```java
import java.util.Objects;
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
public class Pessoa {
    private String nome;
    private int idade;

    public boolean equals(Object other) {
        if(other == null) return false;
        if(this == other) return true;
        if(!(other instanceof Pessoa)) return false;
        Pessoa pessoa = (Pessoa)other;
        if(!Objects.equals(nome, pessoa.nome)) return false;
        if(!Objects.equals(idade, pessoa.idade)) return false;
        return true;
    }
}
```

Ou podemos usar mais lombok ❤️

```java
import lombok.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Pessoa {
    private String nome;
    private int idade;
}
```

A anotação [`EqualsAndHashCode`](https://projectlombok.org/features/EqualsAndHashCode) também cria uma função de hash, veremos isso logo.

---

Em python podemos definir o método [`__eq__`](https://docs.python.org/3/reference/datamodel.html#object.__eq__)
que será utilizado pelo operador `==`.

```python
class Pessoa:

    def __init__(self, nome: str, idade: int):
        self.nome = nome
        self.idade = idade

    def __eq__(self, other):
        if other is None:
            return False
        if self is other:
            return True
        if not isinstance(other, Pessoa):
            return False
        if self.nome != other.nome:
            return False
        if self.idade != other.idade:
            return False
        return True

p1 = Pessoa('Fuu', 23)
p2 = Pessoa('Bar', 23)
p3 = Pessoa('Fuu', 23)

print(p1 == p1)
print(p1 == p2)
print(p1 == p3)
```

Felizmente o decorador `@dataclass` ❤️ já gera esse método.

```python
from dataclasses import dataclass

@dataclass
class Pessoa:
    nome: str
    idade: int

p1 = Pessoa('Fuu', 23)
p2 = Pessoa('Bar', 23)
p3 = Pessoa('Fuu', 23)

print(p1 == p1)
print(p1 == p2)
print(p1 == p3)
```

---

Em C++ podemos definir o método [`operator==`](https://en.cppreference.com/w/cpp/language/operators)
que será utilizado pelo operador `==`.

```cpp

#include <string>
#include <iostream>

class Pessoa {
private:
    std::string nome;
    int idade;
public:
    Pessoa(const char*const nome, int idade):
        nome(nome),
        idade(idade){

        }
    
    bool operator==(const Pessoa& other) const {
        if( this == &other) return true;
        if(this->nome != other.nome) return false;
        if(this->idade != other.idade) return false;
        return true;
    }
};


int main(int argc, char** argv) {
    Pessoa p1("Fuu", 23);
    Pessoa p2("Bar", 23);
    Pessoa p3("Fuu", 23);

    std::cout << (p1 == p1 ? "true" : "false") << std::endl;
    std::cout << (p1 == p2 ? "true" : "false") << std::endl;
    std::cout << (p1 == p3 ? "true" : "false") << std::endl;

}

```

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Projeto Integrador

### Java

- Pesquise como adicionar o lombok no seu projeto
- Adicione o lombok no seu projeto

### Python ou C++

- Pesquise que outros operadores podemos sobrescrever
- Sobrescreva algum operador em seu projeto
