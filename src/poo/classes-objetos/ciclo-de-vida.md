# Ciclo de Vida

> O ciclo de vida dos objetos e os escopos são conceitos fundamentais na programação orientada a objetos (POO) que ajudam a entender como os objetos são criados, usados e eventualmente descartados, além de como a visibilidade das variáveis pode variar dentro de um programa.
>
> O ciclo de vida dos objetos se assemelha ao ciclo de vida de coisas no mundo real. Assim como as plantas crescem, florescem e eventualmente se decompõem, os objetos em POO são criados a partir de uma classe, têm um período de uso ativo e, em algum momento, podem ser removidos da memória. Durante esse ciclo, os objetos podem interagir com outros objetos, receber informações, processar dados e realizar ações específicas. Compreender o ciclo de vida dos objetos é crucial para criar programas eficientes e evitar vazamentos de memória.
>
> Os escopos referem-se ao alcance de visibilidade de uma variável em um programa. Como se fosse um campo de visão, o escopo determina onde uma variável pode ser acessada e usada. Por exemplo, se você define uma variável dentro de uma função, ela não será visível fora dessa função. Isso é semelhante a ter um lugar onde só você pode ver e pegar algo. Os escopos ajudam a organizar e proteger o código, evitando que variáveis sejam acidentalmente modificadas em partes do programa onde não deveriam ser.
>
> Em resumo, compreender o ciclo de vida dos objetos e os escopos é fundamental para escrever programas bem estruturados e eficientes. Isso ajuda a controlar como os objetos são criados e usados, bem como a determinar onde as variáveis podem ser acessadas e modificadas. Ter esse entendimento sólido contribui para a criação de software confiável e de alta qualidade.
>
> Para tentar deixar mais claro quando o método construtor e destrutor são utilizados foram preparados alguns exemplos.
> Nesses exemplos podemos dar nomes aos objetos e esses nomes são impressos durante a construção e durante a destruição.
> [^chat-gpt]

---

## Java

Neste exemplo adicionamos o modificador `static` para o atributo `INSTANCE`, isso significa que esse atributo pertence a classe e não a um objeto,
também podemos encarar como se esse atributo fosse compartilhados entre todos os objetos dessa classe.
O mesmo modificador foi utilizado no método `main`, que é um padrão no java para ser o ponto de entrada do programa.

```java,ocirun
public class Main {

  private String name;

  private static Main INSTANCE = new Main("static");

  Main(String name) {
    this.name = name;
    System.out.println("Construtor: " + name);
  }

  protected void finalize() throws Throwable {
    System.out.println("Destrutor: " + name);
  }

  public static void main(String... args) {
    System.out.println("Hello, World!!!");
    Main a = new Main("a");
    Main b = new Main("b");
    a = null;
    System.gc();
    b = null;
  }
}
```

A primeira construção foi do objeto estático `INSTANCE`, ele foi construído assim que a classe foi definida e em nenhum momento ele foi destruído.
Ele foi removido da memoria junto com o programa, assim o coletor de lixo não teve oportunidade de executar o método `finalize`.

A segunda construção foi do objeto `a`, primeira chamada ao `new Main` no método `main`.
A atribuição de `null` para a variável `a` fez com que não tivéssemos mas nenhuma referência ao objeto `a` em nosso programa, dessa forma ao chamarmos o coletor de lixo com `System.gc()` o objeto `a` foi destruído.

A terceira construção foi do objeto `b`, segunda chamado ao `new Main` no método `main`.
Quando chamamos o coletor de lixo ainda tínhamos uma referência ao objeto e por isso ele não foi destruído.
Fizemos uma atribuição de `null` para a variável `b` logo após a coleta de lixo, assim ficamos sem referencias para esse objeto o tornando apto para coleta na proxima execução.
Como o programa finalizou logo sem seguida a JVM não teve tempo de executar nenhuma coleta de lixo antes de ser removida da memoria.

Podemos ver assim que a utilização do método destrutor em java não é muito confiável,
uma alternativa melhor para liberar recursos após o uso do objeto é com o a interface [`AutoCloseable`](https://docs.oracle.com/javase/8/docs/api/java/lang/AutoCloseable.html).

## C++

Em c++ não temos um coletor de lixo, isso significa que temos que gerenciar manualmente a destruição, ou que assim que a variável sair de escopo, ela será destruída.

```cpp,ocirun
#include <iostream>

class Main {
private:
  const char *const name;

public:
  Main(const char *const name) : name(name) {
    std::cout << "Construtor: " << this->name << std::endl;
  }

  ~Main() { std::cout << "Destrutor: " << this->name << std::endl; }
};

Main global_main("global");

int main(int argc, char **argv) {
  std::cout << "Hello, World" << std::endl;

  Main *c = new Main("c");
  delete c;

  Main a("a");
  Main b("b");
}
```

A primeira construção e ultima destruição é da variável de escopo global, ela inicia e finaliza junto com o programa.

A segunda construção é do objeto `c` que foi iniciado com `new`, que em c++ significa q precisamos usar o `delete` para remove-lo da memoria.

As demais construções foram dos objetos `a` e `b`, que foram destruídos em ordem inversa, um comportamento de pilha.
Estes foram destruídos no fim da execução da função `main`, dona do escopo o qual essas variáveis pertencem.

## Python

Em python também temos um coletor de lixo, mas sua estrategia é um pouco diferente do java.

```python,ocirun
import gc

class Main:

    def __init__(self, name: str):
        self.name = name
        print(f'Construtor: {name}')

    def __del__(self):
        print(f'Destrutor: {self.name}')


if __name__ == '__main__':
    a = Main('a')
    b = Main('b')
    a = None
    # gc.collect()
    print('Hello, World!!!')
```

Podemos observar que mesmo sem chamar o coletor de lixo o objeto foi destruído assim que perdemos sua referência com `a = None`.

## PHP

Em PHP também temos coletor de lixo e sua estrategia é bem semelhante a do python.

```php,ocirun
<?php


class Main
{

    public string $name;

    function __construct(string $name)
    {
        $this->name = $name;
        echo "Construtor: ", $this->name, "\n";
    }

    function __destruct()
    {
        echo "Destrutor: ", $this->name, "\n";
    }
}

$a = new Main("a");
$a = null;
echo "Hello, World!!!", "\n";
//gc_collect_cycles();
$b = new Main("b");

```

---

[^chat-gpt]: <https://chat.openai.com/>

---
