# Modificadores de Acesso

> Na programação orientada a objetos (POO), os modificadores de acesso são ferramentas que definem quais partes do código podem acessar determinados atributos ou métodos de uma classe. Eles controlam a visibilidade e a interação entre os diferentes componentes de um programa.
>
> Imagine que você está construindo uma casa. Existem áreas que todos os membros da família podem acessar livremente, como a sala de estar. Mas existem outras áreas, como o quarto dos pais, que são restritas a pessoas específicas. Os modificadores de acesso são como essas portas e chaves virtuais no mundo da programação. Eles determinam quem pode "entrar" em diferentes partes do código.
>
> Os modificadores de acesso mais comuns são:
>
> - Public: Permite que atributos ou métodos sejam acessados de qualquer lugar, ou seja, de dentro ou fora da classe.
> - Private: Restringe o acesso a atributos ou métodos apenas dentro da própria classe. Outras classes não podem acessá-los diretamente.
> - Protected: Permite que atributos ou métodos sejam acessados dentro da classe e também por suas subclasses, mas não fora delas.
> - Default (Package-private): Quando nenhum modificador é especificado, o acesso é restrito ao pacote (grupo de classes) ao qual a classe pertence.
> Os modificadores de acesso ajudam a criar um ambiente controlado, onde os detalhes internos de uma classe podem ser ocultados do mundo exterior. Isso é importante para a segurança e a manutenção do código, pois evita que partes não autorizadas modifiquem indevidamente o estado de objetos ou acessem informações sensíveis.
>
> Em suma, os modificadores de acesso são como as portas de uma casa, permitindo a regulamentação cuidadosa do acesso aos componentes internos de uma classe. Eles ajudam a manter a organização, segurança e confiabilidade do código em programação orientada a objetos.
> [^chat-gpt]

---

Os modificadores de acesso vão ser o primeiro recurso que vamos usar para fazer encapsulamento,
ou seja, esconder ou proteger detalhes dos nossos objetos.

Os modificadores que mais recomendo a utilização são o `public` e o `private`, para a maioria das linguagens, já em python recomendo a utilização do `public` e do `protected`.

## public

O modificador `public` significa público, qualquer um pode acessar atributos ou métodos públicos.

## protected

O modificador `protected` significa protegido, qualquer classe da mesma familia pode acessar atributos ou métodos protegidos.

## private

O modificador `private` significa privado, somente a classe que declarou os atributos ou métodos pode acessa-los.

## Java

Em java temos os seguintes modificadores de acesso:

- public
- protected
- private
- default

```java

public class Test {
    public int publico;
    protected int protegido;
    private int privado;
    int default;
}

```

## C++

Em C++ temos os seguintes modificadores de acesso:

- public
- protected
- private

```cpp

class Test {
private:
    int privado;
protected:
    int protegido;
public:
    int publico;
}

```

Em C++ também temos a palavra reservada [friend](https://cplusplus.com/doc/tutorial/inheritance/), que serve para dar acesso a propriedades privadas ou protegidas para uma classe ou função amiga.
Veremos um exemplo de uso mais adiante.

## Python

Em python não temos essas palavras reservadas.
Porém temos a convenção de usar `_` para métodos ou atributos protegidos e `__` para privados.
Com apenas um `_` os programadores sabem que devem tomar cuidado com o uso.
O `__` já realmente escode o atributo ou método, o python substitui o nome por um nome mais complicado.
Nenhuma das opções vai realmente impedir o acesso, mas deixa o programador avisado que é por sua conta e risco.

```python
<!-- cmdrun cat examples/acesso.py -->
```

## Quando usar?

- Manter integridade dos dados, expondo apenas métodos "seguros" para modificação dos dados
- Esconder detalhes, ou seja, desacoplar

---

### Conta Bancaria

Imagine uma  classe `Conta` que contem o saldo bancário de alguém.
A conta bancaria mantém o saldo atual, um histórico de movimentações e o titular.

```plantuml
@startuml

class Pessoa {
    + nome: str
}


class Movimento {
    + data: datetime
    + descricao: str
    + origem: Conta
    + valor: float
}

class Conta {
    + titular: Pessoa 
    + saldo: float
    + movimentos: Movimento[]
}

Pessoa "1" --* "0..*" Conta
Movimento "0..*" *--o "1" Conta


@enduml

```

A implementação em python poderia ser algo assim:

```python
<!-- cmdrun cat examples/conta-bancaria-inicial.py -->
```

Essas classes são o suficiente para representar os dados necessários,
mas tem alguns pontos de melhoria.

- Alguém pode alterar o saldo e esquecer de adicionar um movimento

    Qualquer programador poderia alterar o valor do atributo saldo sem criar uma movimentação justificando essa alteração.

- Alguém pode adicionar um movimento e esquecer de atualizar o saldo

    Qualquer programador pode acessar a lista de movimentações e adicionar uma nova movimentação sem necessariamente atualizar o saldo da conta.

- Alguém pode apagar os movimentos

    Qualquer programador pode acessar a lista de movimentações e apagar itens dessa lista. Isso pode tornar o saldo inválido, mas pior ainda é perder o histórico da conta bancaria.

- Alguém pode alterar os movimentos e não atualizar o saldo

    Qualquer programador poderia acessar a lista de movimentações e alterar os registros. Isso torna o saldo inválido e também prejudica a confiabilidade do histórico.

Para resolver esses problemas devemos fornecem maneiras seguras de alterar os dados. Quando falo seguro não é no sentido de proteger contra invasores, mas no sentido de proteger a integridade dos dados no uso cotidiano.
Programadores estão sempre sujeitos a cometer erros, é muito fácil  esquecer de atualizar variáveis dependentes, então devemos fornecer métodos que façam todas as atualizações necessárias.

Ninguém deve alterar o saldo sem adicionar uma movimentação justificando a mudança.
Ninguém deve alterar movimentações já consolidadas, que já alteraram o saldo.
Então devemos bloquear o acesso de escrita para esses atributos.
Primeiramente vamos bloquear todo o acesso, leitura e escrita, tornando esses atributos privados ou protegidos, e então fornecem métodos públicos que fazem somente o permitido, somete ações que mantém a integridade dos dados.

```plantuml
@startuml

class Pessoa {
    + nome: str
}


class Movimento {
    + data: datetime
    + descricao: str
    + origem: Conta
    + valor: float
}

class Conta {
    # titular: Pessoa 
    # saldo: float
    # movimentos: Movimento[]

    + {static} create(Pessoa) : Conta
    + get_titular(): Pessoa
    + get_saldo() : float
    + get_movimentos() : Movimento[]
    + add_movimento(Movimento) -> Conta
}

note top 
    Atributos foram "protegidos"
    Valores acessados por métodos
    que impedem modificações indesejadas
end note

note right of Conta::titular {
    Agora é privado ou protegido
}


note right of Conta::saldo {
    Agora é privado ou protegido
}

note right of Conta::movimentos {
    Agora é privado ou protegido
}

note right of Conta::get_titular {
    Leitura não apresenta risco, pode ser público
}  


note right of Conta::get_saldo {
    Leitura não apresenta risco, pode ser público
}  

note right of Conta::get_movimentos {
    Deve fornecer uma lista imutável ou uma cópia,
    evitando mudanças indesejadas na conta
}  

note right of Conta::add_movimento {
    Efetua a movimentação, insere o movimento na lista
    e atualiza o saldo
}  


Pessoa "1" --* "0..*" Conta
Movimento "0..*" o--o "1" Conta


@enduml

```

Em python a implementação poderia ficar assim:

```python
<!-- cmdrun  cat examples/conta-bancaria-final.py -->
```

Utilizei apenas "protegido", apenas um `_`,
pois o decorator `@dataclass` não funciona com "privado", dois `__`.
O método `get_movimentos` retorna uma cópia dos movimentos,
assim evitamos alterações indesejadas nos movimentos originais.
O método `add_movimento` atualiza a data e a conta do movimento, adiciona o movimento no histórico e finalmente atualiza o saldo da conta.
Esse é único modo de alterar o saldo e o histórico, adicionando um novo movimento a conta.

---

Em C++ poderíamos fazer algo assim:

```cpp
<!-- cmdrun  cat examples/conta-bancaria.cpp -->
```

Em C++ não podemos fazer referencia a uma classe antes de tela declarada, para evitar problemas com isso, criamos as assinaturas das classes, por isso as classes vazias logo no começo.

O método `addMovimento` faz o mesmo que antes, atualiza as informações do movimento, atualiza o saldo e adiciona no histórico.

Os métodos `getTitular`, `getSaldo` e `getMovimentos` tem a palavra `const` após seus nomes, isso significa que são métodos que nao modificam o objeto atual, são métodos imutáveis.
Quando temos um objeto `const` só podemos usar seus métodos que também são `const`.

O método `getMovimentos` retorna a lista de movimentos só que com o `const`, tornando essa lista imutável. Ninguém pode alterar o valor de retorno desse método.

A função `operator<<` foi criada como amiga da classe `Conta`, assim ela pode acessar o atributos privados ou protegidos.
Como essa função tem como parâmetro uma `const Conta`, sabemos que essa função não faz nenhuma modificação no objeto.
Essa função é usada para transformar o objeto conta em um texto no console.

---

Em Java poderíamos fazer algo assim:

```java
<!-- cmdrun  cat examples/conta-bancaria.java -->
```

A implementação é bem semelhante a do python,
protegemos o historio de movimentação fazendo uma copia.
Mas utilizamos a classe [`Stream`](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html) no lugar da classe `List`.

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-acesso.toml}}

---

## Exercícios Práticos

1. Classe para horário

    Defina uma classe para representar horas, minutos e segundos.
    O atributo horas deve sempre estar entre 0 e 23.
    O atributo minutos deve sempre estar entre 0 e 59.
    O atributo segundos deve sempre estar entre 0 e 59.
    Crie métodos para modificar o horário de maneira segura.

## Projeto Integrador

1. Identifique dados/objetos que podem acabar tendo estados inválidos.  
    Exemplos:

    Data e horário:  
    - Dia maior que 31 ou negativo  
    - Mês maior que 12 ou negativo  
    - Dia da semana maior que 7  
    - Horas maior que 23  
    - Minutos maior que 59  
    - Segundos maior que 59

    Distância:
    - Valores negativos
    - Valores maiores do que a capacidade do sensor

    Idade:
    - Valores negativos

    Tempo/Duração:
    - Valores negativos

    Posição de Servo:
    - Entre 0º e 180º

1. Crie classes para garantir a integridade desses dados

    Torne os atributos sensíveis privados ou protegidos e crie métodos públicos para alterar os dados de maneira segura.
    O método construtor pode ser usado para isso também.
