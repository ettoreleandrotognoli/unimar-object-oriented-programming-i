# Prefácio

Este documento foi elaborado pensando nos alunos de Programação Orientada a Objetos I, que já devem ter tido contato ou feito as disciplinas de Algoritmos e Programação de Computadores.
Para quem não se sentir confiante nesses conteúdos recomendo a leitura do capítulo de nivelamento.

Espero que outros professores e alunos possam colaborar com melhorias no material,
contribuindo diretamente no código-fonte, ou com críticas e sugestões.
A licença escolhida da liberdade de redistribuição e alterações, desde que,
tenha citação ao material original e que seja mantida as liberdades da licença original.
Para mais informações verifique a licença.

Espero conseguir transmitir conceitos de orientação a objetos sem ficar preso em uma linguagem de programação específica.
Por isso, tentei colocar exemplos em várias linguagens de programação, pelo menos com as que já tive algum contato.
Também, alguns conceitos são mais fáceis de exemplificar em determinadas linguagens.
A maioria dos exemplos são em Java, Python e C++,
aconselho ter ambientes preparados para programar nessas linguagens.
Apenas ler sobre programação não vai te ajudar tanto quanto praticar programação.

## Objetivo

Espero que no final da disciplina os alunos estejam preparados para aplicar
conceitos básicos de orientação a objeto em [Python](https://www.python.org/), [Java](https://openjdk.org/) e [C++](https://cplusplus.com/),
e se sintam confortáveis para aprender outras linguagens orientadas a objetos.
O aluno deve conseguir analisar e abstrair problemas pensando em objetos,
esboçar e interpretar diagramas [UML](https://www.uml.org/),
ocultar informações entre classes com intuito de diminuir o acoplamento e flexibilizar mudanças,
e por fim, entender a importância e identificar bons designs no processo de desenvolvimento e manutenção de software.

No decorrer das aulas, também espero que os alunos adquiram familiaridade com ambientes de desenvolvimento [opensource](https://opensource.org/), como [Linux](https://www.linuxfoundation.org/), [VSCode](https://github.com/microsoft/vscode) e [GIT](https://git-scm.com/).

### Avaliação

- Frequência

  - 100% de presença: 1 ponto na média do bimestre ( P1, P2 )
  - Apenas uma falta: 0.5 ponto na média do bimestre ( P1, P2 )
  - Outros casos: 0 pontos

- [Projeto Integrador](./projeto-integrador.md)

- Testes surpresas 🤡

## Guia de Colaboração

O código fonte está hospedado no [GitLab](https://gitlab.com/),
então, aconselho a [criação de uma conta](https://gitlab.com/users/sign_up) para realizar contribuições.

### Não sei GIT

Para quem não tem familiaridade com GIT e/ou tem dificuldades com o código fonte,
criticas e sugestões podem ser feitas no [sistema de issues do gitlab](https://gitlab.com/ettoreleandrotognoli/unimar-object-oriented-programming-i/-/issues).

Mas lembre-se, git é uma ferramenta fundamental para qualquer profissional de TI,
aprenda o quanto antes.

- [Git e Github para Iniciantes - Willian Justen](https://www.youtube.com/playlist?list=PLlAbYrWSYTiPA2iEiQ2PF_A9j__C4hi0A>)

### Tenho experiência com GIT

Crie um fork do nosso [projeto](https://gitlab.com/ettoreleandrotognoli/unimar-object-oriented-programming-i) e envie seu *merge request*.

## Licença

[Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR)
