# Padrões de Projeto

> Os padrões de projeto, também conhecidos como design patterns, são soluções comprovadas e recorrentes para problemas comuns de design de software. Eles são abstrações que capturam as melhores práticas de design, facilitando a criação de código limpo, flexível, reutilizável e de fácil manutenção.
>
> Os padrões de projeto surgiram como uma forma de documentar e compartilhar experiências bem-sucedidas na criação de sistemas de software. Eles fornecem um vocabulário comum para os desenvolvedores se comunicarem e colaborarem de forma mais eficiente.
>
> Existem diversos padrões de projeto, divididos em três categorias principais:
>
> Padrões de Criação: Esses padrões se concentram na forma como os objetos são criados, ajudando a isolar a criação de instâncias de classes e tornando o código mais flexível e independente de classes concretas. Exemplos de padrões de criação incluem o Factory Method, Abstract Factory, Singleton e Builder.
>
> Padrões Estruturais: Esses padrões lidam com a organização das classes e objetos para formar estruturas maiores e mais complexas. Eles ajudam a garantir a compatibilidade entre diferentes interfaces e classes. Alguns exemplos de padrões estruturais são o Adapter, Decorator, Facade e Proxy.
>
> Padrões Comportamentais: Esses padrões estão relacionados ao comportamento das classes e objetos e como eles interagem entre si. Eles fornecem soluções para problemas de comunicação e coordenação entre diferentes objetos. Alguns exemplos de padrões comportamentais incluem o Strategy, Observer, Command e Template Method.
>
> A aplicação correta dos padrões de projeto pode levar a benefícios significativos, como a simplificação do código, maior flexibilidade, reutilização de código, melhor separação de responsabilidades e maior facilidade de manutenção. No entanto, é importante lembrar que os padrões de projeto não são soluções universais e devem ser usados com discernimento, aplicando-os onde são apropriados para resolver problemas específicos de design. Cada padrão tem seu contexto adequado e, às vezes, pode ser mais simples e eficaz resolver o problema de forma direta, sem recorrer a um padrão.
> [^chat-gpt]

---

![](./gof-capa.jpg)

## Padrões de Criação

- Factory Method
- Abstract Factory
- Builder
- Prototype
- Singleton ⚠️

## Padrões Estruturais

- Adapter
- Bridge
- Composite
- Decorator
- Facade
- Flyweight
- Proxy

## Padrões Comportamentais

- Chain of Responsibility
- Command
- Iterator
- Mediator
- Memento
- Observer
- State
- Strategy
- Template Method
- Visitor

![](./design-patterns-mindmap.webp)

---

[^chat-gpt]: <https://chat.openai.com/>

<https://refactoring.guru/design-patterns>  
<https://www.youtube.com/playlist?list=PLrhzvIcii6GNjpARdnO4ueTUAVR9eMBpc>  
