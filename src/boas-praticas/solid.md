# SOLID

> O princípio SOLID é um conjunto de cinco princípios de design de software que foram introduzidos por Robert C. Martin para orientar o desenvolvimento de código limpo, flexível e de fácil manutenção. Cada letra do acrônimo SOLID representa um princípio específico, e juntos eles formam uma base sólida para criar sistemas robustos e extensíveis.
>
> Aqui está uma introdução geral sobre cada princípio SOLID:
>
> S - Princípio da Responsabilidade Única (Single Responsibility Principle):
> O SRP enfatiza que uma classe deve ter apenas uma única responsabilidade e motivo para mudar. Isso significa que cada classe deve se concentrar em fazer uma coisa e fazer bem, tornando o código mais coeso, mais fácil de entender e manter.
>
> O - Princípio do Aberto/Fechado (Open/Closed Principle):
> O OCP destaca que as entidades de software (classes, módulos, etc.) devem estar abertas para extensão, mas fechadas para modificação. Isso significa que devemos projetar nossos sistemas de forma que seja possível adicionar novos comportamentos sem alterar o código existente.
>
> L - Princípio da Substituição de Liskov (Liskov Substitution Principle):
> O LSP enfatiza que os objetos de uma classe derivada devem ser substituíveis por objetos de sua classe base sem quebrar o programa. Isso garante que a herança seja usada de forma consistente e que as classes derivadas não quebrem as expectativas das classes base.
>
> I - Princípio da Segregação de Interface (Interface Segregation Principle):
> O ISP preconiza que muitas interfaces específicas são melhores do que uma única interface geral. Isso significa que uma classe não deve ser forçada a depender de interfaces que não utiliza. Isso promove a coesão e evita acoplamentos desnecessários.
>
> D - Princípio da Inversão de Dependência (Dependency Inversion Principle):
> O DIP estabelece que as classes de alto nível não devem depender de classes de baixo nível, mas ambas devem depender de abstrações. Isso incentiva a criação de um código mais flexível e reutilizável, onde as classes de alto nível não estão vinculadas a implementações específicas de classes de baixo nível.
>
> Ao aplicar os princípios SOLID em nossos projetos de software, buscamos criar sistemas mais flexíveis, fáceis de manter, extensíveis e de alta qualidade. Cada princípio SOLID destaca uma abordagem específica para o desenvolvimento de software orientado a objetos, encorajando boas práticas de design e evitando armadilhas comuns que podem levar a código complexo e frágil.
> [^chat-gpt]

---

[^chat-gpt]: <https://chat.openai.com/>

## SRP - Princípio da Responsabilidade Única

Apenas um motivo para mudar

```php,ocirun
<?php

class Book {

    public function __construct(
        protected $author,
        protected $title
        ){
    }

    public function getAuthor(){
        return $this->author;
    }

    public function getTitle(){
        return $this->title;
    }

    public function printBook($format){
        switch ($format) {
            case 'plain-text':
                $text = 'Author: %s, Title: %s';
                echo sprintf($text,$this->author,$this->title),"\n";
                break;
            case 'html':
                $html = '<span>Author: <b>%s</b></span><span>Title: <b>%s</b></span>';
                echo sprintf($html,$this->author,$this->title),"\n";
                break;
            default:
                throw new Exception(sprintf('Format "%s" is not suportted'));
        }
    }

}

$book = new Book('GoF','Design Patterns');
$book->printBook('html');
$book->printBook('plain-text');

```

```php,ocirun
<?php

class Book {

    protected $author;
    protected $title;

    public function __construct($author,$title){
        $this->author = $author;
        $this->title = $title;
    }

    public function getAuthor(){
        return $this->author;
    }

    public function getTitle(){
        return $this->title;
    }

}

interface BookPrinter {

    public function printBook(Book $book);
}

class HTMLBookPrinter {

    public function printBook(Book $book){
        $html = '<span>Author: <b>%s</b></span><span>Title: <b>%s</b></span>';
        echo sprintf($html,$book->getAuthor(),$book->getTitle()),"\n";
    }

}

class PlainTextBookPrinter {
    public function printBook(Book $book){
        $text = 'Author: %s, Title: %s';
        echo sprintf($text,$book->getAuthor(),$book->getTitle()),"\n";
    }
}



$book = new Book('GoF','Design Patterns');
(new HTMLBookPrinter())->printBook($book);
(new PlainTextBookPrinter())->printBook($book);
```

## OCP - Princípio do Aberto/Fechado

Fechadas para modificação e abertas para extensão

```php,ocirun
<?php

date_default_timezone_set('America/Sao_Paulo');
    
class Account {

    protected $owner;
    protected $value;
    protected $audit;

    public function __construct($owner,$value=0,$audit=array()){
        $this->owner = $owner;
        $this->value = $value;
        $this->audit = $audit;
    }

    public function getOwner(){
        return $this->owner;
    }

    public function getValue(){
        return $this->value;
    }

    public function getAudit(){
        return array_values($this->audit);
    }

    public function setValue($newValue,$description='set value'){
        $diff = $this->getValue() - $newValue;
        $this->alterValue($diff,$description);
    }

    public function alterValue($diff,$description='alter value'){
        $oldValue = $this->value;
        $this->value += $diff;
        $log = sprintf('%s: (%s) %.2f, %.2f -> %.2f # %s',
            (new \DateTime())->format(\DateTime::ISO8601),
            $diff >=0 ? '+' : '-',
            abs($diff),
            $oldValue,
            $this->value,
            $description
        );
        $this->audit[] = $log;
    }

    public function pay($value,$title){
        $this->alterValue(-$value,'Payment of '.$title);
    }

    public function receive($value,$title){
        $this->alterValue($value,'Receive of '.$title);
    }

    public function transfer($value,Account $account){
        $this->alterValue(-$value,'Transfer to '.$account->getOwner());
        $account->alterValue($value,'Transfer of '.$this->getOwner());
    }

    public function block(){
        #...
    }

    public function freeze(){
        #...
    }

    public function tax($constValue,$percentValue){
        #...
    }

    #...

}


$accountA = new Account('Fulano',1000);
$accountB = new Account('Ciclano',300);

$accountA->pay(120,'Cable TV');
$accountA->pay(40,'Phone');
$accountA->receive(300,'Bonus');

$accountB->receive(900,'Salary');
$accountB->transfer(400,$accountA);

var_dump($accountA->getAudit());
var_dump($accountB->getAudit());
```

```php
<?php

date_default_timezone_set('America/Sao_Paulo');
    
class Account {

    protected $owner;
    protected $value;
    protected $audit;

    public function __construct($owner,$value=0,$audit=array()){
        $this->owner = $owner;
        $this->value = $value;
        $this->audit = $audit;
    }

    public function getOwner(){
        return $this->owner;
    }

    public function getValue(){
        return $this->value;
    }

    public function getAudit(){
        return array_values($this->audit);
    }

    public function setValue($newValue,$description='set value'){
        $diff = $this->getValue() - $newValue;
        $this->alterValue($diff,$description);
    }

    public function alterValue($diff,$description='alter value'){
        $oldValue = $this->value;
        $this->value += $diff;
        $log = sprintf('%s: (%s) %.2f, %.2f -> %.2f # %s',
            (new \DateTime())->format(\DateTime::ISO8601),
            $diff >=0 ? '+' : '-',
            abs($diff),
            $oldValue,
            $this->value,
            $description
        );
        $this->audit[] = $log;
    }

}

interface AccountAction {

    public function execute(Account $account);

}

class Payment implements AccountAction{

    protected $value;
    protected $title;

    public function __construct($value,$title){
        $this->value = $value;
        $this->title = $title;
    }

    public function execute(Account $account){
        $account->alterValue(-$this->value,'Payment of '.$this->title);
    }
}

class Receivement implements AccountAction{

    protected $value;
    protected $title;

    public function __construct($value,$title){
        $this->value = $value;
        $this->title = $title;
    }

    public function execute(Account $account){
        $account->alterValue($this->value,'Receive of '.$this->title);
    }

}

class Transfer implements AccountAction{

    protected $value;
    protected $toAccount;

    public function __construct($value,$toAccount){
        $this->value = $value;
        $this->toAccount = $toAccount;
    }

    public function execute(Account $account){
        $account->alterValue(-$this->value,'Transfer to '.$this->toAccount->getOwner());
        $this->toAccount->alterValue($this->value,'Transfer of '.$account->getOwner());
    }
}

/*
class Block implements AccountAction{
    ...
}

class Freeze implements AccountAction{
    ...
}

class Tax implements AccountAction{
    ...
}
*/

$accountA = new Account('Fulano',1000);
$accountB = new Account('Ciclano',300);

(new Payment(120,'Cable TV'))->execute($accountA);
(new Payment(40,'Phone'))->execute($accountA);
(new Receivement(300,'Bonus'))->execute($accountA);

(new Receivement(900,'Salary'))->execute($accountB);
(new Transfer(400,$accountA))->execute($accountB);

var_dump($accountA->getAudit());
var_dump($accountB->getAudit());
```

## LSP - Princípio da Substituição de Liskov

> Se q(x) é uma propriedade demonstrável dos objetos x de tipo T.  
> Então q(y) deve ser verdadeiro para objetos y de tipo S onde S é um subtipo de T.

```php,ocirun
<?php

interface Arquivo {

}

class ArquivoPDF implements Arquivo{

    public function gerarPDF(){
        echo 'gerando pdf...',"\n";
    }
}

class ArquivoDOCX implements Arquivo{
    public function gerarDOCX(){
        echo 'gerando docx...',"\n";
    }
}

class ArquivoTXT implements Arquivo{
    public function gerarTXT(){
        echo 'gerando txt...',"\n";
    }
}


class GeradorDeArquivos{
    public function gerarArquivo(Arquivo $arquivo){
        if($arquivo instanceof ArquivoPDF){
            $arquivo->gerarPDF();
        }
        else if($arquivo instanceof ArquivoDOCX){
            $arquivo->gerarDOCX();
        }
        else{
            throw new Exception(sprintf('%s não tem suporte',get_class($arquivo)));
        }
    }
}

$gerador = new GeradorDeArquivos();

try{
    $gerador->gerarArquivo(new ArquivoPDF());
    $gerador->gerarArquivo(new ArquivoDOCX());
    $gerador->gerarArquivo(new ArquivoTXT());
}
catch(Exception $ex){
    echo $ex->getMessage(),"\n";
}

```

```php,ocirun
<?php

class Retangulo {
 
    private $largura;
    private $altura;
 
    public function setAltura($altura) {
        $this->altura = $altura;
    }
 
    public function getAltura() {
        return $this->altura;
    }
 
    public function setLargura($largura) {
        $this->largura = $largura;
    }
 
    public function getLargura() {
        return $this->largura;
    }

    function area() {
        return $this->largura * $this->altura;
    }
 
}

class Quadrado extends Retangulo {
 
    public function setAltura($valor) {
        $this->largura = $valor;
        $this->altura = $valor;
    }
 
    public function setLargura($valor) {
        $this->largura = $valor;
        $this->altura = $valor;
    }
}


class Cliente {
 
    public function verificarArea(Retangulo $r) {
        $r->setLargura(5);
        $r->setAltura(4);
 
        if($r->area() != 20) {
            throw new Exception('Área errada!');
        }
        return true;
    }
 
}

$c = new Cliente();
try{
    $c->verificarArea(new Quadrado());    
}
catch(Exception $ex){
    echo $ex->getMessage(),"\n";
}
```

```php,ocirun
<?php

interface Arquivo {

    public function gerar();
}

class ArquivoPDF implements Arquivo{

    public function gerar(){
        echo 'gerando pdf...',"\n";
    }
}

class ArquivoDOCX implements Arquivo{
    public function gerar(){
        echo 'gerando docx...',"\n";
    }
}

class ArquivoTXT implements Arquivo{
    public function gerar(){
        echo 'gerando txt...',"\n";
    }
}


class GeradorDeArquivos{
    public function gerarArquivo(Arquivo $arquivo){
        $arquivo->gerar();
    }
}

$gerador = new GeradorDeArquivos();

try{
    $gerador->gerarArquivo(new ArquivoPDF());
    $gerador->gerarArquivo(new ArquivoDOCX());
    $gerador->gerarArquivo(new ArquivoTXT());
}
catch(Exception $ex){
    echo $ex->getMessage(),"\n";
}

```

## ISP - Princípio da Segregação de Interface

Clientes não devem depender de interfaces que não usam

```php,ocirun
<?php


interface MessageSender {

    public function emailSend($email,$message);

    public function smsSend($phone, $message);

    public function telegramSend($telegramId,$message);

}


class TelegramSender implements MessageSender{

    public function emailSend($email,$message){
        throw new Exception();
    }

    public function smsSend($phone,$message){
        throw new Exception();
    }

    public function telegramSend($telegramId,$message){
        echo sprintf('telegram-cli -RD -e "msg %s %s"',$telegramId,$message), "\n";
    }    
}

class EMailSender implements MessageSender{
    public function emailSend($email,$message){
        mail($email,'PHP Mail',$message);
    }

    public function smsSend($phone,$message){
        throw new Exception();
    }

    public function telegramSend($telegramId,$message){
        throw new Exception();
    }    
}

class SMSSender implements MessageSender{
    public function emailSend($email,$message){
        throw new Exception();
    }

    public function smsSend($phone,$message){
    }

    public function telegramSend($telegramId,$message){
        throw new Exception();
    }    
}


class MyMessageSender implements MessageSender{

    public function emailSend($email,$message){
        mail($email,'PHP Mail',$message);
    }

    public function smsSend($phone,$message){
    }

    public function telegramSend($telegramId,$message){
        echo sprintf('telegram-cli -RD -e "msg %s %s"',$telegramId,$message), "\n";
    }

}

$sender = new MyMessageSender();
$sender->emailSend('ettore.leandro.tognoli@gmail.com','SOLID');
$sender->smsSend('<numero>','SOLID');
$sender->telegramSend('Éttore_Leandro_Tognoli','SOLID');
```

```php,ocirun

<?php


interface MessageSender {

    public function send($id,$message);

}

class TelegramSender implements MessageSender{
    public function send($telegramId,$message){
        echo sprintf('telegram-cli -RD -e "msg %s %s"',$telegramId,$message), "\n";
    }
}

class EMailSender implements MessageSender{
    public function send($email,$message){
        mail($email,'PHP Mail',$message);
    }
}

class SMSSender implements MessageSender{
    public function send($phone,$message){
    }
}


(new EMailSender())->send('ettore.leandro.tognoli@gmail.com','SOLID');
(new SMSSender())->send('<numero>','SOLID');
(new TelegramSender())->send('Éttore_Leandro_Tognoli','SOLID');
```

## DIP - Princípio da Inversão de Dependência

Todas as dependências de um objeto, o que ele precisa para funcionar,
deve vir pelo construtor.

Módulos de alto nível não devem depender de módulos de baixo nível.
Ambos devem depender de abstrações.

Abstrações não devem depender de detalhes.
Detalhes devem depender de abstrações.

```php,ocirun
<?php

class Lampada {

    protected static $url = 'https://lampadas.com/%d/';

    protected $id;

    public function __construct($id){
        $this->id = $id;
    }

    public function ligar(){
        echo 'PUT ',sprintf(static::$url,$this->id),"\n";
        echo 'estado=1',"\n";
    }

    public function desligar(){
        echo 'PUT ',sprintf(static::$url,$this->id),"\n";
        echo 'estado=0',"\n";
    }

    public function estado(){
        echo 'GET ',sprintf(static::$url,$this->id),"\n";
        return false;
    }

}

class Interruptor {

    protected $lampada;

    public function __construct(){
        $this->lampada = new Lampada(1);
    }

    public function ativar(){
        $this->lampada->ligar();
    }

    public function desativar(){
        $this->lampada->desligar();
    }

}



$interruptor = new Interruptor();
$interruptor->ativar();
$interruptor->desativar();
```

```php,ocirun
<?php

interface Dispositivo {
    public function ligar();
    public function desligar();
}

class Lampada implements Dispositivo{

    protected static $url = 'https://lampadas.com/%d/';

    protected $id;

    public function __construct($id){
        $this->id = $id;
    }

    public function ligar(){
        echo 'PUT ',sprintf(static::$url,$this->id),"\n";
        echo 'estado=1',"\n";
    }

    public function desligar(){
        echo 'PUT ',sprintf(static::$url,$this->id),"\n";
        echo 'estado=0',"\n";
    }

    public function estado(){
        echo 'GET ',sprintf(static::$url,$this->id),"\n";
        return false;
    }
}

class LampadaMock implements Dispositivo{
    protected $id;
    protected $estado;

    public function __construct($id){
        $this->id = $id;
    }

    public function ligar(){
        echo sprintf('Ligando lampada %d',$this->id),"\n";
        $this->estado = true;
    }

    public function desligar(){
        echo sprintf('Desligando lampada %d',$this->id),"\n";
        $this->estado = false;
    }

    public function estado(){
        return $this->estado;
    }

}

class Dispositivos implements Dispositivo {

    protected $dispositivos;

    public function __construct(Array $dispositivos){
        $this->dispositivos = $dispositivos;
    }

    public function ligar(){
        foreach ($this->dispositivos as $dispositivo) {
            $dispositivo->ligar();
        }
    }

    public function desligar(){
        foreach ($this->dispositivos as $dispositivo) {
            $dispositivo->desligar();
        }
    }

}

class Interruptor {

    protected $dispositivo;

    public function __construct(Dispositivo $dispositivo){
        $this->dispositivo = $dispositivo;
    }

    public function ativar(){
        $this->dispositivo->ligar();
    }

    public function desativar(){
        $this->dispositivo->desligar();
    }

}



$interruptor = new Interruptor(new Dispositivos(array(new Lampada(1),new Lampada(2))));
$interruptor->ativar();
$interruptor->desativar();

$interruptor = new Interruptor(new LampadaMock(1));
$interruptor->ativar();
$interruptor->desativar();

```

---

<https://www.youtube.com/watch?v=pTB30aXS77U>  
<https://www.youtube.com/watch?v=pTB30aXS77U>  
<https://www.youtube.com/watch?v=6SfrO3D4dHM>  
<https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898>