# TDD

> Desenvolvimento Orientado a Testes (TDD) é uma abordagem de desenvolvimento de software que coloca os testes no centro do processo de criação de código. A ideia fundamental do TDD é escrever testes automatizados antes de escrever o próprio código da funcionalidade, garantindo que o software seja testado de forma abrangente e contínua à medida que é desenvolvido.
>
> O processo típico do TDD segue três etapas principais:
>
> 1. Escrever Testes: Antes de começar a implementar uma nova funcionalidade ou fazer uma alteração no código existente, o desenvolvedor escreve um teste automatizado que define o comportamento desejado da funcionalidade. Esses testes são frequentemente pequenos e focados em aspectos específicos do código.
>
> 1. Executar Testes: Após a escrita dos testes, o desenvolvedor executa todos os testes existentes. No início, esses testes falharão porque a funcionalidade ainda não foi implementada.
>
> 1. Escrever Código: O desenvolvedor, então, escreve o código necessário para fazer com que os testes passem com sucesso. O objetivo é fazer o código funcionar de acordo com os requisitos definidos pelos testes.
>
> O ciclo se repete continuamente à medida que novas funcionalidades são adicionadas ou o código é modificado. O TDD ajuda a garantir que as alterações no código não quebrem funcionalidades existentes, pois os testes são executados automaticamente sempre que há uma mudança.
>
> O TDD promove a clareza dos requisitos, a documentação viva do código e a confiança na estabilidade do software. Ele também encoraja a criação de código mais modular e fácil de manter, pois os desenvolvedores são incentivados a pensar em como testar suas funcionalidades desde o início do processo de desenvolvimento.
>
> Em resumo, o TDD é uma abordagem de desenvolvimento de software que enfatiza a escrita de testes automatizados antes da implementação do código, garantindo a qualidade, a estabilidade e a manutenibilidade do software ao longo do tempo.
> [^chat-gpt]

---

[^chat-gpt]: <https://chat.openai.com/>

---
