# Bibliografia

Vozes da minha cabeça

## Livros

Arquitetura limpa: O guia do artesão para estrutura e design de software  
Robert C. Martin  
[ISBN-13 978-8550804606](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9788550804606)

Código limpo: Habilidades práticas do Agile Software  
Robert C. Martin  
[ISBN-13 978-8576082675](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9788576082675)

Craftsmanship limpo: disciplinas, padrões e ética  
Robert C. Martin  
[ISBN-13 978-6555209549](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9786555209549)

Domain-Driven Design: Atacando as complexidades no coração do software  
Eric Evans  
[ISBN-13 978-8550800653](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9788550800653)

Extreme Programming Explained: Embrace Change  
Kent Beck, Cynthia Andres  
[ISBN-13 978-0321278654](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9780321278654)

O codificador limpo: Um código de conduta para programadores profissionais  
Robert C. Martin  
[ISBN-13 978-8576086475](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9788576086475)

Padrões de Projetos: Soluções Reutilizáveis de Software Orientados a Objetos  
Erich Gamma, John Vlissides, Richard Helm, Ralph Johnson  
[ISBN-13 978-8573076103](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9788573076103)  

Test Driven Development: By Example  
Kent Beck  
[ISBN-13 978-0321146533](https://www.google.com/search?tbo=p&tbm=bks&q=isbn:9780321146533)

## Documentações

Python 3  
Disponível em <<https://docs.python.org/3/>>

Oracle Java 8 API  
Disponível em <<https://docs.oracle.com/javase/8/docs/api/index.html>>  

Oracle Java 11 API  
Disponível em <<https://docs.oracle.com/en/java/javase/11/docs/api/index.html>>  

CPLUSPLUS  
Disponível em <<https://cplusplus.com/>>
