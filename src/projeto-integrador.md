# Projeto Integrador

O tema dos seus projetos integradores serão definidos por vocês,
os professores vão apenas orientar e sugerir mudanças para atender os requisitos.

Os requisitos gerais serão abordados mais profundamente nas aulas de "Fábrica de Projetos",
mas tenha em mente que o projeto precisa integrar todas as disciplinas do semestre.

A avaliação da disciplina de POO será baseada no Projeto Integrador,
no decorrer das aulas vocês precisam se certificar que o projeto está atendendo os requisitos
listados abaixo.
No final do semestre espera-se que todos, ou pelo menos a maioria, tenham sido atendidos.

Cada aluno, **individualmente**, precisa entregar um relatório justificando como atendeu cada requisito.
Os projetos são em grupo, mas cada aluno tem que elaborar suas próprias justificativas.
Justificativas muito genéricas serão desconsideradas, procure ser especifico,
citar exemplos e anexar artefatos que comprovam a entrega.
Também serão desconsideradas justificativas iguais,
ou seja, não copie e cole de seus colegas.

A nota de P1 inclui as entregas até a data da P1 mais o bônus de frequência do bimestre,
a nota de P2 inclui as demais entregas mais o bônus de frequência do bimestre.

## Conceitos Básicos - 10 Pontos

- [ ] Codificou classes  
    Quais classes? Para que serve?
- [ ] Codificou atributos  
    Em quais classes? Quais atributos? Por que esses nomes e tipos?
- [ ] Codificou métodos  
    Em quais classes? Quais métodos? Por que esses nomes e parâmetros?
- [ ] Codificou atributos estáticos  
    Em quais classes? Por que eles sãos estáticos? Por que esses nomes e tipos?
- [ ] Codificou métodos estáticos  
    Em quais classes? Quais métodos? Por que esses nomes e parâmetros?
- [ ] Codificou métodos construtores  
    Em quais classes?
- [ ] Codificou métodos destrutores [^c++]  
    Em quais classes?
- [ ] Codificou atributos protegidos e/ou privados [^c++] [^java]  
    Em quais classes? Quais atributos? Por que não são públicos?
- [ ] Codificou métodos protegidos e/ou privados [^c++] [^java]  
    Em quais classes? Quais métodos? Por que não são públicos?
- [ ] Codificou interfaces ou classes puramente virtuais  
    Quais classes/interfaces?
- [ ] Codificou classes abstratas ou classes virtuais  
    Quais classes?
- [ ] Instanciou objetos  
    Quais objetos?
- [ ] Instalou e usou bibliotecas de terceiros [^java] [^python]  
    Quais bibliotecas? Para que server?
- [ ] Codificou enums  
    Quais enum?
- [ ] Codificou propriedades [^python]  
    Em quais classes? Quais propriedades?

## Design - 10 Pontos

- [ ] Identificou e codificou classes de dados  
    Quais classes?
- [ ] Identificou e codificou classes de comportamento  
    Quais classes?
- [ ] Usou polimorfismo  
    Com quais classes?
- [ ] Usou objetos imutáveis  
    Quais objetos?
- [ ] Usou diagramas UML para discutir a solução
    Fazer upload dos diagramas
- [ ] Ocultou informações usando atributos e/ou métodos protected/private [^c++] [^java]  
    Em quais classes? Quais atributos e métodos? Por que foi importante ocultar esses dados?
- [ ] Ocultou informações usando interfaces ou classes puramente virtuais  
    Em quais classes/interfaces?
- [ ] Codificou classes imutáveis  
    Quais classes? Por que foi importante elas serem imutáveis?

## Boas Práticas - 10 Pontos

- [ ] Codificou testes unitários  
    Como executo os testes?
- [ ] Codificou padrões de projeto
    Quais padrões? Onde?
- [ ] Usou conceitos de SOLID  
    Quais conceitos? Onde?
- [ ] Usou conceitos de código limpo  
    Quais conceitos? Onde?

## Extras - 1 Ponto cada

- [ ] Versionou todo o projeto integrador com GIT  
    Fazer upload da saída do gitlog
- [ ] Publicou todo projeto integrador no Gitlab, Github, ou semelhantes  
    Enviar link
- [ ] Implantou/Hospedou (deploy) o projeto integrador  
    Enviar link e descrever como foi feito deploy
- [ ] Contribuiu com o material da disciplina criando "issues"  
    Enviar link das issues
- [ ] Contribuiu com o material da disciplina criando PRs/MRs  
    Enviar link dos PRs/MRs
- [ ] Publicou pacotes/bibliotecas do projeto integrador no pypi, maven central, ou semelhantes  
    Enviar link

---

[^c++] Projetos em C++ ou similares

[^java] Projetos em Java ou similares

[^python] Projetos em Python ou similares
