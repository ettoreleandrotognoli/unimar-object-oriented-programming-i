# Operações

--- Explique para uma criança o são operações em programação

> Claro! Vou explicar o que são operações em programação de uma forma simples.
>
> Vamos imaginar que você está brincando com peças de Lego. Você pode fazer várias coisas com essas peças, como empilhá-las, encaixá-las ou separá-las.
>
> Em programação, as operações são como essas ações que podemos fazer com as peças de Lego. São as coisas que podemos fazer com os dados dentro de um programa.
>
> Existem diferentes tipos de operações em programação. Vou te mostrar alguns exemplos:
>
> Operações matemáticas: São as operações que envolvem números, como somar, subtrair, multiplicar e dividir. Por exemplo, se tivermos os números 2 e 3, podemos somá-los para obter o resultado 5.
>
> Operações de comparação: São operações que comparam dois valores e nos dizem se são iguais, maiores ou menores. Por exemplo, podemos comparar se um número é maior que outro, se duas palavras são iguais, etc.
>
> Operações de texto: São operações que trabalham com palavras e textos. Podemos juntar palavras (concatenação), encontrar a quantidade de letras em uma palavra (contagem), entre outras coisas.
>
> Essas são apenas algumas das operações que podemos fazer em programação. Cada tipo de operação tem suas próprias regras e formas de serem usadas.
>
> Quando escrevemos um programa, usamos essas operações para manipular os dados e obter os resultados desejados. É como usar diferentes ferramentas para construir ou transformar coisas.
>
> As operações em programação são como as ações que podemos fazer com as peças de Lego. Elas nos permitem trabalhar com números, palavras e outros tipos de dados para realizar tarefas específicas dentro do programa.
>
> Então, resumindo, as operações em programação são como as ações que podemos fazer com as peças de Lego. São as coisas que podemos fazer com os dados no programa, como somar números, comparar valores ou manipular textos. Elas nos ajudam a realizar tarefas específicas e obter os resultados desejados.
> [^chat-gpt]

---

## Operações Aritméticas

Operações aritméticas são as que geralmente aplicamos em números para fazer algum tipo de cálculo.
As operações ariméticas mais utilizadas foram listadas na tabela a seguir:

|                   |     |
|-------------------|-----|
| Soma              | `+` |
| Subtração         | `-` |
| Multiplicação     | `*` |
| Divisão           | `/` |
| Resto da divisão  | `%` |
| Divisão Inteira   | `//`|
| Potenciação       | `**`|

Potenciação e divisão inteira não são suportadas por todas as linguagens, pelo menos não nesse formato.
Em C/C++, Java e algumas outras linguagens temos uma função para a potenciação invés de uma operação,
mas só vamos abordar funções mais adiante.

As operações aritméticas precisam de dois valores para funcionar, não podemos por exemplo somar um número só.
Os valores usados para uma operação podem ser literais, aqueles que escrevemos diretamente no nosso código, ou podem ser de uma variável.

Soma de literais:

```python
2 + 3
```

Soma de variáveis:

```python
a = 2
b = 3
a + b
```

Soma com literais e variáveis:

```python
a = 2
a + 3
```

Em todos esses exemplos o resultado é o mesmo ( `5` ), mas usamos valores para as [parcelas](https://mundoeducacao.uol.com.br/matematica/adicao.htm) de lugares diferentes.

As operações de soma, subtração, multiplicação, divisão e até mesmo potenciação são muito usadas no dia dia e seguem o mesmo padrão da soma, por isso vou poupar os exemplos.
Já as operações de resto da divisão e divisão inteira podem ser menos usuais.
Quando fazemos uma divisão com números inteiros de forma manual, papel e caneta, conseguimos a divisão inteira, sem virgulas, e também o resto da divisão.
Esses valores podem ser bem uteis em alguns casos, por exemplo, para converter segundos em horas, minutos e segundos.
Podemos usar a divisão inteira para descobrir as horas e usar o resto para calcular os minutos, e seguir a mesma idea para os segundos.

Imagine a situação em que temos um total de 90 minutos, ou seja, 1 hora e 30 minutos.
Podemos usar a divisão inteira de 90 por 60 para chegar em 1 hora,
e usar a o resto da divisão de 90 por 60 para chegar em 30 minutos.
Em python seria algo assim:

```python
minutos = 90
horas = minutos // 60
minutos = minutos % 60
```

O resto da divisão também é muito utilizado para saber se um número é múltiplo do outro.
Para descobrirmos se um número é par, por exemplo, devemos dividir por 2 e verificar o resto da divisão,
se for igual a `0` o número é par.

## Operações Relacionais/Comparação

|                   |     |
|-------------------|-----|
| É igual?          | `==`|
| É diferente?      | `!=`|
| É menor?          | `<` |
| É maior?          | `>` |
| É menor ou igual? | `<=`|
| É maior ou igual? | `>=`|

Essas operações também trabalham com dois valores e sempre resultam em um valor booliano, verdadeiro ou falso.

```
5 > 2
```

Nesse exemplo, podemos ler o código como uma pergunta "5 é maior que 2?", se a resposta for sim, temos o booliano verdadeiro,
se a resposta for não, temos o booliano falso.
Na maioria das linguagens usamos verdadeiro e falso em inglês, `true` e `false`, algumas também podem usar `1` e `0` no lugar.
Muitas vezes as operações relacionais ou de comparação são usadas em conjunto com estruturas de controles, que vamos ver mais a frente,
para decidir se o programa deve ou não executar alguma etapa do algoritmo.
Também podemos atribuir o resultado em alguma variável.

Combinando com a idea do número par, usando resto da divisão por 2, poderíamos escrever algo assim em python:

```python
numero = 5
resto = numero % 2
eh_par = resto == 0
```

## Operações Lógicas

|                   |       |      |
|-------------------|-------|------|
| E                 | `&&`  | `and`|
| Ou                | \|\|  | `or` |
| Não               | `!`   | `not`|

As operações lógicas são usadas com valores boolianos, a lógica "E" e a lógica "Ou" precisam de dois valores, já a lógica "Não" apenas um.
O operação não inverte o valor, verdadeiro se torna falso e falso se torna verdadeiro.

| !a | a |
|----|---|
| T  | F |
| F  | V |

Na lógica "E" todos precisam ser verdade:

| a && b | a  | b |
|--------|----|---|
|    F   | F  | F |
|    F   | F  | V |
|    F   | V  | F |
|    V   | V  | V |

Na lógica "Ou" apenas um precisa ser verdade:

| a \|\| b | a  | b |
|--------|----|---|
|    F   | F  | F |
|    V   | F  | V |
|    V   | V  | F |
|    V   | V  | V |

## Operações de Atribuição

|                               |      |
|-------------------------------|------|
| atribuição                    | `=`  |
| atribuição de adição          | `+=` |
| atribuição de subtração       | `-=` |
| atribuição de multiplicação   | `*=` |
| atribuição de divisão         | `/=` |
| atribuição de divisão inteira | `//=`|
| atribuição de resto           | `%=` |

A operação de atribuição `=` já teve que ser abordada para explicar variáveis, fora ela gosto de chamar as outras operações de [açúcar sintático](https://pt.wikipedia.org/wiki/A%C3%A7%C3%BAcar_sint%C3%A1tico), pois é apenas um jeito mais prático de escrever algo que geralmente seria maior.

Algumas vezes precisamos escrever algo assim:

```python
a = a + 10
```

A variável `a` recebe seu próprio valor mais `10`.
Um exemplo clássico seria na soma de um carrinho de compras:

```python
total_carrinho = 0
total_carrinho = total_carrinho + valor_produto_1
total_carrinho = total_carrinho + valor_produto_2
total_carrinho = total_carrinho + valor_produto_3
```

Para simplificar podemos substituir `a = a + 10` por:

```python
a += 10
```

Tornando o código do carrinho de compras menor:

```python
total_carrinho = 0
total_carrinho += valor_produto_1
total_carrinho += valor_produto_2
total_carrinho += valor_produto_3
```

O mesmo vale para as demais operações.

## Operações Bit-a-Bit/Bitwise

Espero que ainda lembrem das aulas de lógica computacional e algebra booliana.
Essas operações são muito uteis quando estamos trabalhando em baixo nível,
ou "escovando bits" como alguns diriam.
Podemos controlar cada bit com essas operações,
mas para isso precisamos pensar na representação binária de cada número.

|                        |     |
|------------------------|-----|
| não                    | `~` |
| e                      | `&` |
| ou                     | \|  |
| xor                    | `^` |
| deslocar para esquerda | `<<`|
| deslocar para direita  | `>>`|

Operações bit-a-bit  também são úteis quando precisamos trabalhar com "flags",
mas usando pouco memória,
com apenas um `int` podemos representar 32 boolianos com está técnica.

Os deslocamentos podem ser usados para fazer multiplicação ou divisão por 2.
Isso funciona semelhante ao deslocamento de virgula, ou colocar zeros a esquerda,
mas como o computador trabalha na base 2 no lugar da base 10, estamos multiplicando ou dividindo por 2.

```python
a = 5 << 1
# resulta em 10, 5 * 2
a = 5 << 5
# resulta em 160, 5 * 2 * 2 * 2 * 2 * 2
```

```python
a = 20 >> 1
# resulta em 10
a = 20 >> 3
# resulta em 2, 20 // 2 // 2 // 2
```

Essas operações também podem ser combinadas com as operações de atribuição:

|                        |      |
|------------------------|------|
| e                      | `&=` |
| ou                     | \|=  |
| xor                    | `^=` |
| deslocar para esquerda | `<<=`|
| deslocar para direita  | `>>=`|

## Outras operações

Ainda temos as operações `++` e `--`, que equivalentes á `+=1` e `-=1`.
Porém, o `++` e o `--` podem ser usados na direita ou na esquerda da variável,
isso muda o retorno, mas não o resultado salvo na variável.
Com o operador a esquerda da variável, o retorno é o valor após o incremento ou decremento,
com o operador a direita da variável, o retorno é o valor antes do incremento ou decremento.

```cpp,ocirun
#include <iostream>

int main(int argc, char** argv) {
    int a = 0;
    int b = ++a;
    int c = a++;

    std::cout << "a = " << a << std::endl;
    std::cout << "b = " << b << std::endl;
    std::cout << "c = " << c << std::endl;
}

```

É uma mudança sútil, mas pode ter efeitos interessantes na memória.
Com o operador a direita, precisamos de dois valores em memória, o atual, após incremento ou decremento,
e o anterior, sem incremento ou decremento, para usar como resultado.

Infelizmente esses operadores não existem em python.

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-operacoes.toml}}

---

## Dúvidas frequentes
