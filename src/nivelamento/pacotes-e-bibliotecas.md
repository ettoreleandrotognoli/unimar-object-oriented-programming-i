# Pacotes e Bibliotecas

--- Explique para uma criança o que são pacotes e bibliotecas em programação

> Claro! Vamos explicar de forma simples:
>
> Imagine que você está brincando com suas pecinhas de montar (LEGO). Cada pecinha é como uma parte do programa que você quer criar. Às vezes, você precisa de muitas pecinhas para fazer algo legal e divertido, como construir uma casinha.
>
> Agora, pense nos pacotes e bibliotecas como caixinhas cheias de pecinhas especiais que outras pessoas já montaram para você! Essas caixinhas têm pecinhas diferentes e interessantes, que podem fazer coisas incríveis, como fazer um carro andar ou criar um robô dançante.
>
> Quando você quer usar essas pecinhas prontas, você abre a caixinha (pacote ou biblioteca) e pega as pecinhas que precisa. Isso é como usar os pacotes e bibliotecas em programação!
>
> Em vez de criar tudo do zero, podemos usar pacotes e bibliotecas para fazer nosso programa mais rápido e com menos esforço. Assim, podemos construir coisas legais de forma mais fácil e rápida!
> [^chat-gpt]

---

Diversas vezes vamos tentar resolver problemas tão comuns,
que muitos outros programadores já passaram por ele.
Provavelmente alguns deles foram gentis e publicaram suas soluções,
assim, podemos usar soluções já testadas em nossos programas.

Alguns problemas são tão comuns que já temos a solução embutida na linguagem de programação.
Essas soluções são disponibilizadas através de pacotes, bibliotecas, módulos, componentes e etc.
Existem diversos nomes, cada linguagem pode fazer suas próprias distinções entre esses nomes.
Mas meu foco aqui é falar sobre como usar código que não escrevemos e separar nosso código em outros arquivos para melhorar nossa organização.

## Python

Em python temos as palavras reservadas `from`, `import` e `as`,
que podemos usar para importar elementos de outros arquivos.

```python
import modulo
from modulo import variavel
from pacote.modulo import variavel
from pacote import modulo

import modulo as apelido
from modulo import variavel as apelido
from pacote.modulo import variavel as apelido
from pacote import modulo as apelido
```

### Nativos

As linguagens de programação geralmente já possuem algumas bibliotecas embutidas,
a comunidade do python as chama de baterias incluídas.

A seguir alguns exemplos dessas bibliotecas:

#### Matemática

```python
import math
math.cos(math.pi / 4)
# 0.70710678118654757
math.log(1024, 2)
# 10.0
```

```python
import random
random.random()    # random float
# 0.17970987693706186
```

#### Data & Hora

```python
from datetime import date
now = date.today()
now
#datetime.date(2003, 12, 2)
now.strftime("%m-%d-%y. %d %b %Y is a %A on the %d day of %B.")
#'12-02-03. 02 Dec 2003 is a Tuesday on the 02 day of December.'
# dates support calendar arithmetic
birthday = date(1964, 7, 31)
age = now - birthday
age.days
# 14368
```

#### Compressão

```python
import zlib
s = b'witch which has which witches wrist watch'
len(s)
# 41
t = zlib.compress(s)
len(t)
# 37
zlib.decompress(t)
# b'witch which has which witches wrist watch'
```

#### Leitura de Senha

```python
from getpass import getpass

getpass()
```

#### Criptografia

```python
import hashlib
m = hashlib.sha256()
m.update(b"Nobody inspects")
m.update(b" the spammish repetition")
m.digest()
#b'\x03\x1e\xdd}Ae\x15\x93\xc5\xfe\\\x00o\xa5....
m.hexdigest()
#'031edd7d41651593c5fe5c006....

```

<https://docs.python.org/pt-br/3/tutorial/stdlib.html>

### Estrutura

Para criarmos nossos próprios pacotes é simples,
precisamos criar uma hierarquia de pastas e arquivos.

Segue exemplo:

- `./`
  - `main.py`
  - `lib.py`
  - `pacote`
    - `outra_lib.py`

#### ./pacote/outra_lib.py

```python
def multiplica(a, b):
    return a * b
```

#### ./lib.py

```python
def soma(a, b):
    return a + b
```

Depois dos pacotes serem criados podemos os importar
usando `from` e `import` seguindo a hierarquia de pastas e arquivos.

#### ./main.py

```python
from lib import soma
from pacote.outra_lib import multiplica

soma(2, 2)
multiplica(2, 2)
```

## Gerenciamento de Dependências

Algumas vezes podemos querer usar soluções que não estão embutidas na linguagem,
a melhor maneira de fazer isso é através de uma ferramenta de gerenciamento de dependências.
Essas ferramentas fazem todo o trabalho de identificar nossas dependências, baixar e instalar.
Cada linguagem tem suas próprias ferramentas de gerenciamento de dependência,
geralmente com uma ferramenta de construção, que compila nosso código, embutida.

- Python
  - poetry
  - pip
  - pipenv
  - anaconda

- Java
  - maven
  - gradle
  - ant

- C/C++
  - conan
  - make
  - cmake

- javascript

  - npm
  - yarn

- PHP

  - composer

Vamos deixar mais detalhes desse assuntos para as aulas de Engenharia de Software.

## Publicação

Além de podermos disponibilizar o código fonte em sites, como github, gitlab e etc,
podemos publicar os pacotes em servidores oficias das ferramentas de gerenciamento de dependência,
assim outros programadores podem baixar e instalar facilmente nossos pacotes.

- [maven central](https://mvnrepository.com/)
- [conan center](https://conan.io/center)
- [npm](https://www.npmjs.com/)
- [pypi](https://pypi.org/)
- [crates](https://crates.io/)
- [packagist](https://packagist.org/)

---

[^chat-gpt]: <https://chat.openai.com/>

<!--

## Exercícios de fixação

---

## Dúvidas frequentes

-->
