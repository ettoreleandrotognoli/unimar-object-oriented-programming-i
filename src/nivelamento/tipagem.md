
# Tipagem

— Explique para uma criança o que é tipagem em programação

> Claro! Vou explicar o que é tipagem em programação de uma forma simples.
>
> Imagine que você está organizando seus brinquedos em caixas. Para facilitar, você decide separar os brinquedos em diferentes categorias. Por exemplo, você coloca os carrinhos em uma caixa, as bonecas em outra caixa e os jogos de tabuleiro em uma terceira caixa.
>
> A tipagem em programação é como separar os diferentes tipos de informações que usamos em nossos programas. Cada tipo de informação tem características diferentes e precisa ser tratado de maneira especial.
>
> Por exemplo, em programação, podemos ter diferentes tipos de dados, como números, palavras e listas de coisas. Cada tipo de dado tem suas próprias regras e comportamentos.
>
> Quando escrevemos um programa, precisamos dizer ao computador que tipo de dado estamos usando. Isso é chamado de declaração de tipo. É como escrever uma etiqueta na caixa dos brinquedos para dizer o que está dentro.
>
> A tipagem pode ser dividida em dois tipos principais: tipagem estática e tipagem dinâmica.
>
> Tipagem estática: É como quando você separa seus brinquedos em caixas e coloca uma etiqueta em cada caixa para dizer qual tipo de brinquedo está ali. No momento em que escrevemos o código, precisamos dizer ao computador qual é o tipo de dado que estamos usando. O computador verifica se estamos usando corretamente esse tipo de dado durante a compilação do programa.
>
> Tipagem dinâmica: É como quando você decide mudar um brinquedo de uma caixa para outra enquanto brinca. Nesse caso, o computador verifica o tipo de dado em tempo de execução, ou seja, enquanto o programa está rodando. Podemos trocar o tipo de dado que uma variável contém durante a execução do programa.
>
> A tipagem ajuda o computador a entender melhor o que estamos fazendo e a evitar erros. Ela também nos permite trabalhar com diferentes tipos de informações de maneira adequada e realizar as operações corretas com elas.
>
> Em resumo, a tipagem em programação é como organizar nossos dados em diferentes categorias, assim como você organiza seus brinquedos em caixas. Isso nos ajuda a escrever programas corretos e a lidar com diferentes tipos de informações de maneira apropriada.
> [^chat-gpt]

---

Assim como as variáveis possuem nomes elas também possuem tipo e o tipo define as operações que podemos realizar, o espaço e o como essas variáveis ocupam a memoria.

Como já foi dito, os computadores são muito bons em realizarem operações matemáticas, então vou usar multiplicação como exemplo.

```python
a = 2
b = a * 5
```

Neste exemplo em python estamos declarando uma variável `a` iniciando com o valor `2`.
Como não colocamos o `2` entre `"` ou `'` a variável `a` é do tipo `int`.
Na segunda linha estamos fazendo a multiplicação de `a` por `5` usando o operador `*` de multiplicação e armazenando o resultado em `b`.
Espero então que no final do meu programa a variável `b` tenha o valor `10` também do tipo `int`.

Agora se mudarmos o tipo da variável `a` para o tipo `str` teremos um resultado bem diferente.

```python
a = '2'
b = a * 5
```

Neste exemplo estamos declarando uma variável `a` iniciando com o valor `'2'`, ou seja, uma string, tipo `str`.
Na segunda linha estamos fazendo a "multiplicação" de `a` por `5` usando o operador `*` de multiplicação e armazenando o resultado em `b`, assim como no exemplo anterior.
Porém, não existe a operação matemática de um texto multiplicado por um número, em algumas linguagens isso poderia criar um erro, ou forçar uma conversão do texto para número, o que nem sempre é possível.
No caso do python a "multiplicação" de um texto por um número resulta na repetição do texto, nesse caso a variável `b` deve conter o valor `'22222'`, o valor de `a` repetido `5` vezes.

Espero que isso deixe claro que os tipos definem o comportamento das variáveis.
As mesmas operações aplicadas em tipos diferentes tem resultados diferentes.

Os tipos também definem como e o espaço que ocupam em memoria, em linguagens como C/C++, rust, assembly e etc conseguimos controlar muito bem isso.
Em C/C++ por exemplo temos o tipo `char` que por padrão utiliza 8 bits ou 1 byte,
podendo representar números de -128 até 127.
Podemos adicionar a palavra `unsigned` ao tipo `char`,
formando um `unsigned char` que ocupa o mesmo espaço mas não representa números negativos,
assim representando números de 0 até 255.
Em C/C++ também temos o tipo `int`, que além de podemos usar `unsigned`, podemos usar `short` para diminuir o tamanho e `long` para aumentar. [^cpp-ref]

Na tabela abaixo temos os tipos mais comuns de dados e seus respectivos nomes nas linguagens Java, C/C++ e Python.

|                                            | Java            | C/C++             | Python |
|--------------------------------------------|-----------------|-------------------|--------|
| Verdadeiro ou Falso ( Booliano )           | bool            | bool              | bool   |
| Números inteiros muitos pequenos ( ~ 8bits)| byte            | char              | int    |
| Números inteiros pequenos ( ~ 16 bits)     | short           | short             | int    |
| Números inteiros ( ~ 32 bits)              | int             | int               | int    |
| Números inteiros grandes ( ~ 64 bits)      | long            | long              | int    |
| Números reais ( ~ 32 bits)                 | float           | float             | float  |
| Números reais grandes/precisos ( ~ 64 bits)| double          | double            | float  |
| Caractere/Letra                            | char            | char              | str    |
| Texto/String                               | String ou char[]| string ou char*   | str    |

[^w3-java] [^w3-python]

---

[^chat-gpt]: <https://chat.openai.com/>

[^cpp-ref]: <https://en.cppreference.com/w/cpp/language/types#Range_of_values>

[^w3-java]: <https://www.w3schools.com/java/java_data_types.asp>

[^w3-python]: <https://www.w3schools.com/python/python_datatypes.asp>

---

## Exercícios de fixação

{{#quiz ./quiz-tipagem.toml}}

---

## Dúvidas frequentes
