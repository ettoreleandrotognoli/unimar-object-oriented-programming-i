total_alunos = 10

notas = []

for indice in range(total_alunos):
    print('Digite a nota do aluno ', indice, ':')
    nota = float(input())
    notas.append(nota)

media = 0
for nota in notas:
    media += nota
media /= total_alunos

print('Media: ', media)

for indice, nota in enumerate(notas):
    if nota < media:
        print('aluno ', indice, ' abaixo da media')
