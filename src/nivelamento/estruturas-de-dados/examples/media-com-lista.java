import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Main {

  public static void main(String... args) {
    Scanner scanner = new Scanner(System.in);
    int totalAlunos = 10;
    List<Float> notas = new ArrayList<>();
    for (int indice = 0; indice < totalAlunos; ++indice) {
      System.out.println("Digite a nota do aluno " + indice);
      float nota = scanner.nextFloat();
      notas.add(nota);
    }
    float media = 0;
    for (int indice = 0; indice < totalAlunos; ++indice) {
      media += notas.get(indice);
    }
    media /= totalAlunos;
    System.out.println("Media: " + media);
    for (int indice = 0; indice < totalAlunos; ++indice) {
      if (notas.get(indice) < media) {
        System.out.println("aluno " + indice + " abaixo da media");
      }
    }
  }
}
