# Erros e Exceções

--- Explique para uma criança o que é tratamento de exceções em programação

> Claro! Vou explicar de forma simples:
>
> Imagine que você está brincando de jogar bola com seus amigos. Quando você joga a bola, seus amigos podem ou pegá-la sem problemas ou, às vezes, a bola pode cair no chão e você precisa buscar de novo.
>
> Na programação, as coisas também podem dar errado de vez em quando. Por exemplo, quando um programa está tentando fazer alguma coisa, como dividir um número por zero (o que não é permitido em matemática), ele pode encontrar um problema. Esse problema é chamado de "exceção".
>
> O tratamento de exceções é como você e seus amigos lidam com a bola que cai no chão. Em vez de deixar o programa travar e parar de funcionar quando encontra uma exceção, você ensina o programa a pegar a exceção e lidar com ela de uma forma especial.
>
> Assim como você busca a bola quando ela cai no chão e continua o jogo sem problemas, o tratamento de exceções permite que o programa identifique o erro, saiba o que fazer quando isso acontece e continue funcionando corretamente.
>
> Dessa forma, o tratamento de exceções ajuda a tornar os programas mais robustos e evita que eles parem de funcionar de repente quando algo inesperado acontece. É uma maneira de garantir que o programa continue executando suas tarefas sem se atrapalhar com problemas que possam surgir no meio do caminho.
> [^chat-gpt]

---

Boa parte das linguagens tem um "if else" especial, especifico para erros,
geralmente sendo chamado de "try catch", ou "try except".
A ideia é tentar ( try ) fazer algo,
caso aconteça um erro, o capturamos (catch) e fazemos as tratativas necessárias.
Algumas linguagens combinaram mais elementos com o "try catch", falaremos desses elementos na seção de cada linguagem.

## Python

Primeiramente vamos entender o que é um erro em python,
ou o que chamamos de exceção.
Em python tempos um tipo para representar esse erro,
que é a classe `Exception`.
Dependendo do tipo de erro, usamos um exceção diferente,
mas ainda assim é uma exceção.
Quando entrarmos no assunto de orientação a objetos vou poder detalhar mais.

Um erro "comum" que muitas vezes só vamos perceber em tempo de execução é a divisão por zero.
Para isso temos a exceção [`ZeroDivisionError`](https://docs.python.org/3/library/exceptions.html#ZeroDivisionError).
Segue exemplo:

```python,ocirun
sete = 7
zero = 0
resultado = sete / zero
print(resultado)
```

Mais comum ainda é confiar equivocadamente nas entradas fornecidas pelo usuário:

```python
<!-- cmdrun cat examples/input-erro.py -->
```

Agora imagine que o usuário escreva isso:

```console
<!-- cmdrun cat examples/input-erro-in.txt  -->
```

O resultado seria:

```console
<!-- ocirun python python examples/input-erro.py < examples/input-erro-in.txt -->
```

Nesse exemplo temos um [`ValueError`](https://docs.python.org/3/library/exceptions.html#ValueError).

Nos dois casos as funções que chamamos, `/` e`int` ,dispararam uma exceção,
elas não chegam nem retornar um resultado,
o lançamento da exceção não tratado interrompe a execução do programa
e escreve na tela detalhes sobre o erro.
Muito provavelmente não vamos querer mostrar todos esses detalhes para o usuário.
Também podemos contornar esses erros e entregar algo mais confiável para o usuário.

```python,ocirun
sete = 7
zero = 0
try:
    resultado = sete / zero
except:
    print('Sinto muito, tivemos um erro de cálculo')
else:
    print(resultado)
finally:
    print('Fim do try/except')
print('Tchau')
```

O bloco `try` tem nossa divisão por zero,
o bloco `except` é executado para qualquer exceção,
o bloco `else` caso não tenha exceção,
e o `finally` sempre é executado no final.
Os blocos `else` e `finally` são opcionais.

Também podemos definir quais exceções queremos capturar e tratar.

```python
try:
    a = float(input())
    b = float(input())
    c = a / b
except ValueError as error:
    print('Parece que você digitou algo errado')
    print(error)
except ZeroDivisionError as error:
    print('Não podemos dividir por zero')
    print(error)
```

Nesse exemplo definimos o tipo da exceção que queremos tratar logo após o
`except`, também, armazenamos a exceção em uma variável usando `as`.
Assim `except ValueError as error`, significa capturar qualquer `ValueError` e armazenar em `error`.

<https://www.w3schools.com/python/python_try_except.asp>

Além de tratar as exceções com o `try except`, podemos disparar nossas próprias exceções.
Para disparar uma exceção usamos a palavra reservada `raise`.

Espero que vocês lembrem, pelo menos vagamente, da formula de bhaskara.
Nela calculamos a raiz quadrada de delta,
mas algumas vezes delta pode ser negativo,
significando que a equação não tem raízes reais.

```python,ocirun
def bhaskara(a, b, c):
    delta = b ** 2 - 4 * a * c
    if delta < 0:
        raise Exception("Números negativos não tem raiz quadrada")
    return (-b + delta ** 0.5) / (2 * a), (-b - delta ** 0.5) / (2 * a)

bhaskara(3, 2, 2)
```

## C++

Em C++ usamos `try catch` para tratar a exceção e `throw` para disparar.

```cpp,ocirun
#include <array>
#include <iostream>
#include <cmath>

using namespace std;

array<float, 2> bhaskara(float a, float b, float c) {
  float delta = powf32(b, 2) - 4 * a * c;
  if (delta < 0) {
    throw "Números negativos não tem raiz quadrada";
  }
  return array<float, 2>{
      (-b + sqrtf32(delta)) / (2 * a),
      (-b - sqrtf32(delta)) / (2 * a),
  };
}

int main(int argc, char **argv) {
  try {
    bhaskara(3, 2, 2);
  } catch (char const *mensagem) {
    cout << mensagem << endl;
  }
  bhaskara(3, 2, 2);
}
```

<https://www.w3schools.com/cpp/cpp_exceptions.asp>

---

[^chat-gpt]: <https://chat.openai.com/>

<!--

## Exercícios de fixação

-->

---

## Dúvidas frequentes

- Tem alguma função semelhante a função `input` mas que já trata a entrada seguindo alguma regra?

    Não encontrei nenhuma função pronta para isso.
    Provavelmente porque quando tratamos entradas desse tipo geralmente já estamos trabalhando com interfaces gráficas.
    Mas seria possível fazer uma função desse tipo, segue material que pode ajudar:  
    <https://docs.python.org/3/library/re.html>  
    <https://pypi.org/project/readchar/>  

---

<!-- @todo exemplo com Exception generico -->
