# Nomes

— Explique para uma criança o que é nome de uma variável em programação

> Claro! Vou explicar o que é o nome de uma variável de uma forma simples.
>
> Imagine que você tem um amiguinho imaginário e quer dar um nome para ele. Esse nome é como um apelido que você usa para chamar seu amiguinho sempre que precisa se referir a ele.
>
> Da mesma forma, uma variável em programação também precisa ter um nome. Esse nome é uma palavra que você escolhe para identificar a variável dentro do programa. É como se fosse um apelido que você dá para a variável.
>
> Por exemplo, vamos supor que você quer criar uma variável para guardar a quantidade de maçãs que você tem. Você pode escolher um nome para essa variável, como "quantidade_de_macas".
>
> ```makefile
> quantidade_de_macas = 5
> ```
>
> Aqui, "quantidade_de_macas" é o nome que você escolheu para a variável. Sempre que quiser se referir à quantidade de maçãs, você usa esse nome.
>
> É importante escolher um nome que faça sentido e seja fácil de entender. Assim, quando você olhar para o nome da variável, saberá do que se trata.
>
> Então, o nome de uma variável em programação é como um apelido que você dá para a variável, para poder se referir a ela de maneira fácil e organizada dentro do programa.
> [^chat-gpt]

---

Cada linguagem de programação tem suas regras e convenções para os nomes das variáveis.
Regras nós somos obrigados a seguir, se não nosso programa não funciona.
Convenções devemos seguir para facilitar a leitura do nosso código.

Grande parte das linguagens segue a regra de que o nome das variáveis devem começar com uma letra, ou simbolo, como `$` e `_`, mas nunca um número.
Isso ajuda a diferenciar o nome de um valor literal.
Em C/C++, por exemplo, podemos adicionar um sufixo no valor para forçar o tipo ou um prefixo para definir o formato.
O literal `1f` significa o número `1` usando com o tipo `float`.
O literal `0b100` significa o número `100` em binário, ou seja, `4` em decimal.

Como convenção normalmente não usamos acentuação nos nomes das variáveis, algumas linguagens também não suportam, ou seja, dependendo da linguagem isso é uma regra.

A maiorias das linguagens de programação fazem distinção de letras maiúsculas e minusculas,
o que chamamos de "[Case-Sensitive](https://pt.wikipedia.org/wiki/Case-sensitive)",
ou seja, a variável `Nome` não é a mesma coisa que a variável `nome` ou `NOME`.

No exemplo a seguir estamos criando uma variável chamada `x` do tipo inteiro.
O nome `x` é apenas uma letra, ou seja segue a regra de não começar com um número.

```cpp
int x;
```

Já em Python precisamos atribuir um valor para variável assim que a criamos.

```python
x = 0
```

Mesmo `x` sendo um nome valido para a variável, raramente `x` será um bom nome.
Devemos sempre dar nomes que atribuam significado, que nos ajude a entender o porquê da variável existir e/ou como vamos usa-lá.
Para isso muitas vezes precisamos usar nomes compostos, mas não podemos usar espaços nos nomes.
Como solução juntamos as palavras com `_` ( [snake_case](https://en.wikipedia.org/wiki/Snake_case) ), ou deixamos a primeira letra de cada palavra maiúscula ( [CamelCase](https://pt.wikipedia.org/wiki/CamelCase) ).

Seguem alguns exemplos seguindo as convenções de cada linguagem:

## Java

```java
float salarioMedioDosFuncionarios;
```

<https://www.w3schools.com/java/java_variables.asp>  
<https://docs.oracle.com/javase/tutorial/java/nutsandbolts/variables.html>  

## C/C++

```c++
float salario_medio_dos_funcionarios;
```

<https://www.w3schools.com/cpp/cpp_variables.asp>  
<https://www.geeksforgeeks.org/naming-convention-in-c/>  

## Python

```python
salario_medio_dos_funcionarios = 0.
```

<https://www.w3schools.com/python/gloss_python_variable_names.asp>

## PHP

A linguagem de programação [PHP](https://www.php.net/) tem o diferencial de identificar as variáveis com um `$`,
toda variável deve ter o `$` como prefixo.

```php
<?php

$salario_medio_dos_funcionarios = 0;
```

<https://www.w3schools.com/php/php_variables.asp>  

## javascript + jQuery

Em javascript existe um framework famoso, o [jQuery](https://jquery.com/),
por convenção ele usa a variável `$`.

```js
var saladio_medio_dos_funcionarios = 0;
var $ = jQuery;
```

<https://www.w3schools.com/js/js_variables.asp>  

---

[^chat-gpt]: <https://chat.openai.com/>

---

## Exercícios de fixação

{{#quiz ./quiz-nomes-variaveis.toml}}

---

## Dúvidas frequentes
