import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String... args) {
        Random random = new Random();
        random.setSeed(13);
        Scanner scanner = new Scanner(System.in);
        boolean brincarDeNovo;
        do {
            int numeroSecreto = random.nextInt(100);
            System.out.println("Tente adivinhar o número Secreto, [0, 99)");
            int chute = -1;
            while(chute != numeroSecreto) {
                chute = scanner.nextInt();
                if(chute != numeroSecreto) {
                    System.out.println("Errou");
                }
                if(numeroSecreto > chute) {
                    System.out.println("Mais...");
                }
                else if (numeroSecreto < chute) {
                    System.out.println("Menos...");
                }
            }
            System.out.println("Acertou miserável");
            System.out.println("Gostaria de brincar de novo?");
            brincarDeNovo = "sim".equals(scanner.next().trim().toLowerCase());
        }
        while(brincarDeNovo);
    }
    
}
