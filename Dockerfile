FROM rust:latest
RUN cargo install \
    mdbook-emojicodes \
    mdbook-plantuml \
    mdbook-quiz \
    mdbook-plantuml \
    mdbook-cmdrun \
    mdbook-ocirun
RUN cargo install --git https://github.com/ettoreleandrotognoli/mdBook mdbook
COPY --from=docker:dind /usr/local/bin/docker /usr/local/bin/